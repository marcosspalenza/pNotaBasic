"""
pNota 2021.
Author: Marcos A. Spalenza

"""
import os
import docker
import scipy as sp
import numpy as np
import pandas as pd
from pNota_preprocessing import DataIO
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import pairwise, silhouette_samples

class Clustering:

    _METRICS = [
        'cityblock', 'cosine', 'euclidean', 'braycurtis', 'canberra', 'chebyshev',
        'correlation', 'dice', 'hamming', 'jaccard', 'kulsinski', 'mahalanobis', 'matching',
        'minkowski', 'rogerstanimoto', 'russellrao', 'sokalmichener', 'sokalsneath', 'yule'
    ]

    _ALGORITHMS = ["agglomerative"]

    _INDEXES = ["silhouette", "ch_score", "db_score", "sse", "cv_size", "cv_distance"]

    _PYTHON_MOD = "python3"
    _CLSTRLOC = "/home/mspalenza/pNota/clustering_optimization/clstr/"
    _IMAGE = "marcosspalenza/clustering_opt:test"

    def __init__(self, data_, inputpath_, outputpath_):
        self.data = sp.sparse.csr_matrix(data_)
        self.inputpath = outputpath_
        self.outputpath = outputpath_+"/clusters/"
        DataIO().instance().output_check(self.outputpath)

    def _read_train_files(self):
        # load train files
        if os.path.isfile(self.inputpath+"/minmax.mat"):
            minmax = DataIO().instance().read_document("minmax.mat", pathto=self.inputpath).split("\n")
            minmax = np.unique([int(tr.split(" ")[2]) for tr in minmax if tr != ""]+[int(tr.split(" ")[3]) for tr in minmax if tr != ""]).tolist()
            return minmax
        else:
            return []

    def _validate_files(self):
        # check clustering files' consistence
        if not os.path.isfile(self.outputpath+"clusters.txt"):
            return False
        elif not os.path.isfile(self.outputpath+"exec.csv"):
            return False
        elif not os.path.isfile(self.inputpath+"clstr.cfg"):
            return False
        elif not os.path.isfile(self.outputpath+"cluster_0.mat"):
            return False
        if not os.path.isfile(self.inputpath+"minmax.mat"):
            return False
        else:
            return True


    def _rank_clusters(self, clstr):
        """
        Analyze clustering results, to collect most diverse clusters (avoiding small clusters < 10)
        """
        clstr = pd.read_csv(self.outputpath+"exec.csv", sep="\t", index_col=False)
        partitions = [sum([3 if int(p_) > 10 else 1 for p_ in p.split(":") ]) for p in clstr.sort_values(by=["CVS"])["Clusters"].values]
        clstr_ix = [pi for pi, p in enumerate(partitions) if p == max(partitions)][0]
        best = clstr.sort_values(by=["CVS"]).values[clstr_ix]
        parameters = [
            "source = pNota", "exec_id = "+str(clstr_ix), "algorithm = "+str(best[4]),
            "ivi = CoeffVariationSize", "score = "+str(best[11]), "time = "+str(best[2]), 
            "metrics = "+str(best[3]), "k_clusters = "+str(len(best[16].split(":"))),
            "cluster_distrib = "+str(best[16])
        ]
        DataIO().instance().save_document("clstr.cfg", parameters, pathto=self.inputpath)
        return best, clstr_ix


    def run_clustering(self, module="docker", mount_clusters=True):
        cluster_best = []
        cluster_ix = []
        clstr_out = []
        clusters = []

        if mount_clusters:
            if self._validate_files():
                clusters = DataIO().instance().read_document("clusters.txt", pathto=self.outputpath).split("\n")

        if clusters == []:
            # remove cluster files
            for cfile in os.listdir(self.outputpath):
                os.remove(self.outputpath+cfile)
            if os.path.isfile(self.inputpath+"clstr.cfg"):
                os.remove(self.inputpath+"clstr.cfg")
            if os.path.isfile(self.inputpath+"minmax.mat"):
                os.remove(self.inputpath+"minmax.mat")

            DataIO().instance().save_csr_matrix("data.mtx", self.data, pathto=self.inputpath)

            try:
                if module == "docker":
                    for m in self._METRICS:
                        for i in self._INDEXES:
                            client = docker.from_env()
                            drivers = {self.inputpath: {'bind': '/data/input/', 'mode': 'rw'},
                                        self.outputpath: {'bind': '/data/output/', 'mode': 'rw'}}

                            cmd = "python3 main_clustering.py -f sparse -d "+m+" -e "+i+" data.mtx"
                            client.containers.run(image=self._IMAGE, command=cmd, volumes=drivers, network=None, remove=True)

                elif module == "clstr":
                    for m in self._METRICS:
                        for i in self._INDEXES:
                            cmd = self._PYTHON_MOD+" "+self._CLSTRLOC+"main_clustering.py -i "+self.inputpath+" -o "+self.outputpath+" -f sparse -d "+m+" -e "+i+" data.mtx"
                            os.system(cmd)

                clstr_out = DataIO().instance().read_csv_document("exec.csv", pathto=self.outputpath, header=True, std_separator='\t')

                clusters = DataIO().instance().read_document("clusters.txt", pathto=self.outputpath).split("\n")

            except Exception as e:
                if os.path.isfile(self.outputpath+"exceptions.csv"):
                    err = DataIO().instance().read_csv_document("exceptions.csv", pathto=self.outputpath, header=True, std_separator='\t')[-1]
                    print(err)
                else:
                    print("[Error] Clustering methods not avaliable : "+str(e))
        else:
            clstr_out = DataIO().instance().read_csv_document("exec.csv", pathto=self.outputpath, header=True, std_separator='\t')

        if clusters != [] and clstr_out != []:
            cluster_best, index = self._rank_clusters(clstr_out)
            cluster_ix = [int(c) for c in clusters[index].split()]

        return cluster_best, cluster_ix


    def train_test_split(self, clusters, train_mode=30, selection="silhcoeff"):
        train = []
        test = []
        minmax_selection = []

        parameters = DataIO().instance().read_document("clstr.cfg", pathto=self.inputpath).split("\n")

        metric = [p.split("=")[1].strip() for p in parameters if "metrics" in p.split("=")[0]][0]

        distance_matrix = pairwise.pairwise_distances(self.data.todense(), metric=metric)

        distance_matrix = MinMaxScaler().fit_transform(distance_matrix)

        train = self._read_train_files()

        if train != []:
            if len(np.unique(train)) == int((train_mode*len(clusters))/100):
                test = [ix for ix, _ in enumerate(clusters) if ix not in train]
                return train, test
        
        """
        Cluster sampling modes [2 / 4 / 6] items
        """
        mode_select = [m for m in [2, 4, 6] if m*len(np.unique(clusters)) < (train_mode*len(clusters))/100]
        
        if (train_mode * len(clusters))/100 > 3 and len(mode_select) > 0:
            mode = mode_select[-1]
        else:
            mode = 2

        for c_ in np.unique(clusters):
            clstr = [cid for cid, c in enumerate(clusters) if c == c_]

            clstritems = []
            for ix1 in clstr:
                for ix2 in clstr:
                    if ix1 < ix2:
                        distance = distance_matrix[ix1, ix2]
                        clstritems.append(str(ix1)+" "+str(ix2)+" "+str(distance))

            DataIO().instance().save_document("cluster_"+str(c_)+".mat", clstritems, pathto=self.outputpath)

            if mode >= 2:
                sim = distance_matrix[clstr, : ]
                sim = sim[ : , clstr] 
                tr1, tr2 = np.unravel_index(np.argmax(sim, axis=None), sim.shape)
                train.append(clstr[tr1])
                train.append(clstr[tr2])
                minmax_selection.append("{0} maxsim {1} {2} {3}\n".format(c_, clstr[tr1], clstr[tr2], sim[tr1, tr2].item()))

            if mode >= 4:
                sim = distance_matrix[clstr, : ]
                sim = sim[ : , clstr] 
                np.fill_diagonal(sim, 2.0)
                tr1, tr2 = np.unravel_index(np.argmin(sim, axis=None), sim.shape)
                train.append(clstr[tr1])
                train.append(clstr[tr2])
                minmax_selection.append("{0} minsim {1} {2} {3}\n".format(c_, clstr[tr1], clstr[tr2], sim[tr1, tr2].item()))

            if mode >= 6:
                answ_size = np.sum(self.data[clstr, :], axis=1)
                tr1 = np.argmin(answ_size, axis=None)
                tr2 = np.argmax(answ_size, axis=None)
                train.append(clstr[tr1])
                train.append(clstr[tr2])
                minmax_selection.append("{0} minsize {1} {2} {3}\n".format(c_, clstr[tr1], clstr[tr1], 1.0))
                minmax_selection.append("{0} maxsize {1} {2} {3}\n".format(c_, clstr[tr2], clstr[tr2], 1.0))

        test = [ix for ix, _ in enumerate(clusters) if ix not in train]
        trvalues = int((train_mode/100)*len(clusters))

        train = np.unique(train).tolist()

        if len(train) != int((train_mode*len(clusters))/100):
            if selection == "weighted":
                """
                Selecting items by weighted clustering size
                """
                while len(train) < trvalues:
                    citems = [(ix, c) for c in enumerate(clusters) if ix not in train]
                    clix, clval = zip(*citems)
                    cprob = np.cumsum([float(len([c_ for c_ in clval if c == c_])) for c in np.unique(clval)])
                    rdn = np.random.rand()
                    selected_clstr = clval[[ix for ix, csum in enumerate(cprob) if csum >= rdn][-1]]
                    te = [ix for ix, c in enumerate(citems) if c == selected_clstr]
                    rdn = int(np.random.randint(0, len(te)-1))
                    minmax_selection.append("{0} wgtselect {1} {2} {3}\n".format(selected_clstr, te[rdn], te[rdn], 1.0))
                    train.append(te[rdn])
                test = [ix for ix, c in enumerate(clusters) if ix not in train]

            elif selection == "silhcoeff":
                """
                Selecting items by silhouette coefficient inter-cluster samples' position
                """
                te = list(np.unique(test))
                coeff = silhouette_samples(self.data, clusters) # metric = metric)
                citems = [ix for ix, c in enumerate(clusters) if ix not in train]
                silh = [(citems[ci], cf) for ci, cf in sorted(enumerate([coeff[ix] for ix in citems]), key = lambda x:x[1])]
                while len(train) < trvalues:
                    ix, cf = silh[0]
                    train.append(ix)
                    minmax_selection.append("{0} silhcoeff {1} {2} {3}\n".format(clusters[ix], ix, ix, cf))
                    silh = silh[1:]
                test = [ix for ix, c in enumerate(clusters) if ix not in train]

            else:
                """
                Selecting items randomly
                """
                te = list(np.unique(test))
                while len(train) < trvalues:
                    rdn = int(random.randint(0, len(te)-1))
                    train.append(te[rdn])
                    minmax_selection.append("{0} rdnselect {1} {2} {3}\n".format(clusters[te[rdn]], te[rdn], te[rdn], 1.0))
                    te = te[:rdn]+te[rdn+1:]
                
                test = [ix for ix, c in enumerate(clusters) if ix not in train]

        DataIO().instance().save_document("minmax.mat", minmax_selection, pathto=self.inputpath)
        return train, test
