"""
pNota 2021.
Author: Marcos A. Spalenza

"""
# native and third-part libraries
import os
import sys
import time
import socket
import logging # outputmanager requisite
import resource # outputmanager requisite
import warnings
import numpy as np
import wisardpkg as wsd
from sklearn.svm import SVC
from argparse import ArgumentParser # arguments manager
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.preprocessing import binarize, KBinsDiscretizer
from sklearn.linear_model import LinearRegression, Lasso
from sklearn.metrics import accuracy_score, mean_squared_error, cohen_kappa_score
from sklearn.tree import DecisionTreeRegressor, DecisionTreeClassifier
from sklearn.neighbors import KNeighborsRegressor, KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier

# pNota files
from pNota_report import Report
from pNota_preprocessing import DataIO, Dataset


'''
Main Method
'''
__STD_CLASSIFIER = "GBC"
__STD_REGRESSOR = "DTRG"
LOG_FILENAME = "pNota.log"

def main():
    '''
    args Parser for first step processing
    '''
    parser = ArgumentParser(usage="%(prog)s [args] <datasetdir>",  description="pNota: A Short Answer Scoring System, Sampling by Clustering Analysis")
    parser.add_argument("dataset", help="Dataset folder.")
    parser.add_argument("-s", "--dbstructure", dest="data_structure", default="moodle_files", help="Dataset input structure. Default: moodle_files. [ moodle_files, single_file, just_files ]")
    parser.add_argument("-f", "--filename", dest="data_file", default="resposta.txt", help="Input file name. Default: resposta.txt.")
    parser.add_argument("-d", "--trdoc", dest="train_document", default="notastreino.csv", help="Data train/test document. Default: notastreino.csv.")
    parser.add_argument("-e", "--encoding", dest="encoding", default="utf-8", help="Dataset encoding for all files. Default: utf-8. [ iso-8859-1, utf-8 ]")
    parser.add_argument("-l", "--language", dest="language", default="portuguese", help="Dataset language. Default: portuguese. [ portuguese, english, spanish ]")
    parser.add_argument("-g", "--grades", dest="grade_model", default="discrete", help="Grading model. Default: discrete. [ ordinal, discrete, continuous ]")
    parser.add_argument("-a", "--apply_grader", dest="apply_grader", default="enabled", help="Generate new grades in test samples. Default: enabled. [ enabled, disabled ]")
    parser.add_argument("-i", "--individual_reports", dest="individual_reports", default="disabled", help="Generate dataset individual reports. Default: disabled. [ enabled, disabled ]")
    args = parser.parse_args()

    '''
    Args Verification
    '''
    dataset_directory = args.dataset
    output_directory = dataset_directory+"/logs_pNota/"
    DataIO().instance().output_check(output_directory)

    print("- INPUT \t : "+dataset_directory)
    print("- OUTPUT \t : "+output_directory)
    print(args)

    '''
    Output Manager 
        Add a custom formatter to root logger for output information
    '''
    logging.basicConfig(filename=output_directory+LOG_FILENAME, format='%(asctime)s \t %(message)s', datefmt='%d/%m/%Y %I:%M:%S', level=logging.DEBUG)
    handler = logging.StreamHandler()
    logging.getLogger().addHandler(handler)
    logger = logging.getLogger()

    try:
        if os.stat(output_directory+LOG_FILENAME).st_ctime < time.time() - 7 * 86400:
            os.remove(output_directory+LOG_FILENAME)
            with open(output_directory+LOG_FILENAME, "a") as wtr:
                wtr.write("pNota @ "+str(socket.gethostname())+"\n\n")
    except:
        logger.info("[Warning] \t File [run.log] not found! A new [run.log] was automatically created.")

    '''
    Data IO
    '''
    logger.info("[pNota @ "+str(socket.gethostname())+"] Started: Method [Classification] for "+[d for d in dataset_directory.split('/') if d != ""][-1])

    data_methods = Dataset(args.data_structure, language_=args.language, encoding_=args.encoding)
    data = data_methods.get_dataset(dataset_directory, output_directory, filename=args.data_file)

    if data == []:
        logger.error("[Error] \t Dataset empty.")
        sys.exit()

    students, responses = zip(*data)

    if len(students) < 10:
        logger.error("[Info] \t Waiting complete dataset. Document size < 10.")
        sys.exit()

    dbmatrix = DataIO().instance().read_csr_matrix("data.mtx", pathto=output_directory)

    features = DataIO().instance().read_document("wordlist.txt",  pathto=output_directory).split("\n")

    features = [f.split("\t")[0] for f in features] #ignore feature's matrix frequencies

    train_doc = DataIO().instance().read_csv_document("notastreino.csv", pathto=dataset_directory, header=False, std_separator=';')

    docnames = [t.split("\t")[0] for tix, t in enumerate(train_doc)]

    print(len(students))
    print(len(docnames))
    if [s for s in students if str(s) not in docnames] != []:
        logger.error("[Error] \t Unavaliable student grades.")
        sys.exit()

    tritems = []
    tsitems = []

    if args.grade_model == "ordinal":
        tritems = [(tix, t.split("\t")[1]) for tix, t in enumerate(train_doc) if "-1" not in t.split("\t")[1]]
        tsitems = [(tix, t.split("\t")[1]) for tix, t in enumerate(train_doc) if "-1" in t.split("\t")[1]]

    elif args.grade_model == "discrete":
        tritems = [(tix, float(t.split("\t")[1])) for tix, t in enumerate(train_doc) if float(t.split("\t")[1]) > -1]
        tsitems = [(tix, float(t.split("\t")[1])) for tix, t in enumerate(train_doc) if float(t.split("\t")[1]) <= -1]

    elif args.grade_model == "continuous":
        tritems = [(tix, float(t.split("\t")[1])) for tix, t in enumerate(train_doc) if float(t.split("\t")[1]) > -1]
        tsitems = [(tix, float(t.split("\t")[1])) for tix, t in enumerate(train_doc) if float(t.split("\t")[1]) <= -1]

    else:
        logger.error("[Error] \t Unrecognized grading format.")
        sys.exit()

    required_train = DataIO().instance().read_document("minmax.mat",  pathto=output_directory).split("\n")

    if required_train == []:
        logger.info("[Error] \t Unespected empty train subset log file")
        sys.exit()

    required_train = [tix.split(" ") for tix in required_train if tix != ""]

    required = []
    train = []
    test = []
    for tix in required_train:
        required.append(int(tix[2]))
        required.append(int(tix[3]))

    required_train = np.unique(required)

    """
    Train Test Dataset Split
    """
    tr_names = [students[tix] for tix in required_train]

    train = [ix for ix, grade in tritems]
    test = [ix for ix, grade in tsitems]

    """
    Check train index
    """
    if len(train) == 0:
        logger.info("[Info] \t Waiting complete train database.")
        sys.exit()

    if len(train) < len(required_train):
        logger.info("[Info] \t Waiting complete train database. Train: "+
            str(len(train))+" Request: "+str(len(required_train)))
        sys.exit()
    if args.apply_grader == "disabled" and len(test) == 0:
        logger.info("[Info] \t Empty test set.")
        sys.exit()

    if [d_ for d_ in docnames if d_ not in students] != []:
        logger.error("[Error] \t Unavaliable train requirements."+
            str([d_ for d_ in docnames if d_ not in students]))
        sys.exit()

    if [d_ for d_ in students if d_ not in docnames] != []:
        logger.error("[Error] \t Unavaliable train requirements."+
            str([d_ for d_ in students if d_ not in docnames]))
        sys.exit()

    if (len(train) + len(test)) != len(students):
        logger.error("[Error] \t Unrecognized items inside train or documents list.")
        sys.exit()

    logger.info("[Info] \t Train size: "+str(len(train))+" \t\t Test size: "+str(len(test)))
    logger.info("[Info] \t Train (%): "+str(100*round(len(train)/len(students), 4))+" \t\t Test (%): "+str(100*round(len(test)/len(students), 4)))


    """
    Classifiers / Regressors
    """
    test_class = True

    train_x = dbmatrix[train, :]
    test_x = dbmatrix[test, :]

    std_clf = ""
    results = []

    train_y = [grade for tix, grade in tritems]

    if len(np.unique(train_y)) < 2:
        results = [train_y[0] for ix, _ in enumerate(test_x)]

    elif args.grade_model == "discrete" or args.grade_model == "ordinal":

        train_y = [str(lbl).strip() for lbl in train_y]

        binX = binarize(dbmatrix).squeeze().astype(int).tolist()

        std_clf = __STD_CLASSIFIER

        if test_class == True:    

            warnings.filterwarnings("ignore", category=UserWarning)

            skf = StratifiedKFold(2).split(train_x, train_y)

            std_best = .0
            for trix, vaix in skf:
                try:
                    clf = SVC()
                    clf.fit(train_x[trix, :], [train_y[ix] for ix in trix])
                    va_y = clf.predict(train_x[vaix, :])
                    kappa = cohen_kappa_score([train_y[ix] for ix in vaix], va_y, weights="quadratic")
                    logger.info("[Info] \t SKF-2: SVM : Kappa "+str(kappa))
                    if kappa > std_best:
                        std_best = kappa
                        std_clf = "SVM"

                    clf = DecisionTreeClassifier()
                    clf.fit(train_x[trix, :], [train_y[ix] for ix in trix])
                    va_y = clf.predict(train_x[vaix, :])
                    kappa = cohen_kappa_score([train_y[ix] for ix in vaix], va_y, weights="quadratic")
                    logger.info("[Info] \t SKF-2: DTR : Kappa "+str(kappa))
                    if kappa > std_best:
                        std_best = kappa
                        std_clf = "DTR"

                    clf = GradientBoostingClassifier()
                    clf.fit(train_x[trix, :], [train_y[ix] for ix in trix])
                    va_y = clf.predict(train_x[vaix, :])
                    kappa = cohen_kappa_score([train_y[ix] for ix in vaix], va_y, weights="quadratic")
                    logger.info("[Info] \t SKF-2: GBC : Kappa "+str(kappa))
                    if kappa > std_best:
                        std_best = kappa
                        std_clf = "GBC"

                    clf = RandomForestClassifier()
                    clf.fit(train_x[trix, :], [train_y[ix] for ix in trix])
                    va_y = clf.predict(train_x[vaix, :])
                    kappa = cohen_kappa_score([train_y[ix] for ix in vaix], va_y, weights="quadratic")
                    logger.info("[Info] \t SKF-2: RDF : Kappa "+str(kappa))
                    if kappa > std_best:
                        std_best = kappa
                        std_clf = "RDF"

                    clf = KNeighborsClassifier(3)
                    clf.fit(train_x[trix, :], [train_y[ix] for ix in trix])
                    va_y = clf.predict(train_x[vaix, :])
                    kappa = cohen_kappa_score([train_y[ix] for ix in vaix], va_y, weights="quadratic")
                    logger.info("[Info] \t SKF-2: KNN : Kappa "+str(kappa))
                    if kappa > std_best:
                        std_best = kappa
                        std_clf = "KNN"

                    clf = wsd.Wisard(15)
                    ds_train = wsd.DataSet([binX[ix] for ix in trix], [train_y[ix] for ix in trix])
                    ds_test = wsd.DataSet([binX[ix] for ix in vaix])
                    clf.train(ds_train)
                    va_y = clf.classify(ds_test)
                    kappa = cohen_kappa_score([train_y[ix] for ix in vaix], va_y)
                    logger.info("[Info] \t SKF-2: WSD : Kappa "+str(kappa))
                    if kappa > std_best:
                        std_best = kappa
                        std_clf = "WSD"

                except Exception as e:
                    logger.info(e)
        try:
            clf = SVC()
            clf.fit(train_x, train_y)
            test_y = clf.predict(test_x)
            DataIO().instance().save_document("svm.labels", test_y, pathto=output_directory)
            if std_clf == "SVM":
                logger.info("[Info] \t Classifier SVM")
                results = test_y
        except Exception as e:
            logger.error(e)

        try:
            clf = DecisionTreeClassifier()
            clf.fit(train_x, train_y)
            test_y = clf.predict(test_x)
            DataIO().instance().save_document("dtr.labels", test_y, pathto=output_directory)
            if std_clf == "DTR":
                logger.info("[Info] \t Classifier DTREE")
                results = test_y
        except Exception as e:
            logger.error(e)

        try:
            clf = GradientBoostingClassifier()
            clf.fit(train_x, train_y)
            test_y = clf.predict(test_x)
            DataIO().instance().save_document("gbc.labels", test_y, pathto=output_directory)
            if std_clf == "GBC":
                logger.info("[Info] \t Classifier Gradient Boosting")
                results = test_y
        except Exception as e:
            logger.error(e)

        try:
            clf = RandomForestClassifier()
            clf.fit(train_x, train_y)
            test_y = clf.predict(test_x)
            DataIO().instance().save_document("rdf.labels", test_y, pathto=output_directory)
            if std_clf == "RDF":
                logger.info("[Info] \t Classifier Random Forest")
                results = test_y
        except Exception as e:
            logger.error(e)

        try:
            clf = KNeighborsClassifier(3)
            clf.fit(train_x, train_y)
            test_y = clf.predict(test_x)
            DataIO().instance().save_document("knn.labels", test_y, pathto=output_directory)
            if std_clf == "KNN":
                logger.info("[Info] \t Classifier K-NN")
                results = test_y
        except Exception as e:
            logger.error(e)

        try:
            clf = wsd.Wisard(15)
            ds_train = wsd.DataSet([binX[ix] for ix in train], train_y)
            ds_test = wsd.DataSet([binX[ix] for ix in test])
            clf.train(ds_train)
            test_y = np.array(clf.classify(ds_test))
            DataIO().instance().save_document("wsd.labels", test_y, pathto=output_directory)
            if std_clf == "WSD":
                logger.info("[Info] \t Classifier WiSARD")
                results = test_y
        except Exception as e:
            logger.error(e)

    elif args.grade_model == "continuous":

        train_y = [float(lbl) for lbl in train_y]
        y_max, y_min = max(train_y), min(train_y)

        binX = binarize(dbmatrix).squeeze().astype(int).tolist()

        std_clf = __STD_REGRESSOR

        if test_class == True:
            skf = KFold(2).split(train_x)

            std_best = float('inf')
            for trix, vaix in skf:
                try:
                    reg = LinearRegression()
                    reg.fit(train_x[trix, :], [train_y[ix] for ix in trix])
                    va_y = reg.predict(train_x[vaix, :])
                    corr = np.corrcoef([train_y[ix] for ix in vaix], va_y)[0, 1]
                    logger.info("[Info] \t KF-2: LINR : CorrP "+str(corr))
                    if corr > std_best:
                        std_best = corr
                        std_clf = "LINR"

                    reg = Lasso()
                    reg.fit(train_x[trix, :], [train_y[ix] for ix in trix])
                    va_y = reg.predict(train_x[vaix, :])
                    corr = np.corrcoef([train_y[ix] for ix in vaix], va_y)[0, 1]
                    logger.info("[Info] \t KF-2: LASSO : CorrP "+str(corr))
                    if corr > std_best:
                        std_best = corr
                        std_clf = "LSSR"

                    reg = DecisionTreeRegressor()
                    reg.fit(train_x[trix, :], [train_y[ix] for ix in trix])
                    va_y = reg.predict(train_x[vaix, :])
                    corr = np.corrcoef([train_y[ix] for ix in vaix], va_y)[0, 1]
                    logger.info("[Info] \t KF-2: DTRG : CorrP "+str(corr))
                    if corr > std_best:
                        std_best = corr
                        std_clf = "DTRG"

                    reg = KNeighborsRegressor(3)
                    reg.fit(train_x[trix, :], [train_y[ix] for ix in trix])
                    va_y = reg.predict(train_x[vaix, :])
                    corr = np.corrcoef([train_y[ix] for ix in vaix], va_y)[0, 1]
                    logger.info("[Info] \t KF-2: KNRG : CorrP "+str(corr))
                    if corr > std_best:
                        std_best = corr
                        std_clf = "KNRG"

                    reg = wsd.RegressionWisard(15)
                    ds_train = wsd.DataSet([binX[ix] for ix in trix], [train_y[ix] for ix in trix])
                    ds_test = wsd.DataSet([binX[ix] for ix in vaix])
                    reg.train(ds_train)
                    va_y = np.array(reg.predict(ds_test))
                    corr = np.corrcoef([train_y[ix] for ix in vaix], va_y)[0, 1]
                    logger.info("[Info] \t KF-2: WSDREG : CorrP "+str(corr))
                    if corr > std_best:
                        std_best = corr
                        std_clf = "WSRG"

                except Exception as e:
                    logger.info(e)

        try:
            reg = LinearRegression()
            reg.fit(train_x, train_y)
            test_y = reg.predict(test_x)
            
            for ix, _ in enumerate(test_y):
                if test_y[ix] > y_max:
                    test_y[ix] = y_max
                if test_y[ix] < y_min:
                    test_y[ix] = y_max
            
            DataIO().instance().save_document("linr.labels", [str(t) for t in test_y], pathto=output_directory)
            if std_clf == "LINR":
                logger.info("[Info] \t Regressor LINEAR REG")
                results = test_y
        except Exception as e:
            logger.error(e)

        try:
            reg = Lasso()
            reg.fit(train_x, train_y)
            test_y = reg.predict(test_x)

            for ix, _ in enumerate(test_y):
                if test_y[ix] > y_max:
                    test_y[ix] = y_max
                if test_y[ix] < y_min:
                    test_y[ix] = y_max

            DataIO().instance().save_document("lssr.labels", [str(t) for t in test_y], pathto=output_directory)
            if std_clf == "LSSR":
                logger.info("[Info] \t Regressor LASSO REG")
                results = test_y
        except Exception as e:
            logger.error(e)

        try:
            reg = DecisionTreeRegressor()
            reg.fit(train_x, train_y)
            test_y = reg.predict(test_x)

            for ix, _ in enumerate(test_y):
                if test_y[ix] > y_max:
                    test_y[ix] = y_max
                if test_y[ix] < y_min:
                    test_y[ix] = y_max

            DataIO().instance().save_document("dtrg.labels", [str(t) for t in test_y], pathto=output_directory)
            if std_clf == "DTRG":
                logger.info("[Info] \t Regressor DTREE REG")
                results = test_y
        except Exception as e:
            logger.error(e)

        try:
            reg = KNeighborsRegressor(3)
            reg.fit(train_x, train_y)
            test_y = reg.predict(test_x)

            for ix, _ in enumerate(test_y):
                if test_y[ix] > y_max:
                    test_y[ix] = y_max
                if test_y[ix] < y_min:
                    test_y[ix] = y_max

            DataIO().instance().save_document("knrg.labels", [str(t) for t in test_y], pathto=output_directory)
            if std_clf == "KNRG":
                logger.info("[Info] \t Regressor K-NN REG")
                results = test_y
        except Exception as e:
            logger.error(e)

        try:
            reg = wsd.RegressionWisard(15)
            ds_train = wsd.DataSet([binX[ix] for ix in train], train_y)
            ds_test = wsd.DataSet([binX[ix] for ix in test])
            reg.train(ds_train)
            test_y = np.array(reg.predict(ds_test))

            for ix, _ in enumerate(test_y):
                if test_y[ix] > y_max:
                    test_y[ix] = y_max
                if test_y[ix] < y_min:
                    test_y[ix] = y_max

            DataIO().instance().save_document("wsrg.labels", [str(t) for t in test_y], pathto=output_directory)
            if std_clf == "WSRG":
                logger.info("[Info] \t Regressor WiSARD")
                results = test_y
        except Exception as e:
            logger.error(e)

    """

    Generate the document containing the final grades

    |notastreino.csv         |
    |------------------------|
    | #Id | Grade | Feedback |
    |------------------------|
    """
    if len(results) != len(test):
        # grader (classifier) and test samples inconsistencies
        print()
        logger.error("[Error] \t Inconsistent sizes between test and grade items ["+str(len(test))+" : "+str(len(results))+"]")
        sys.exit()

    grades = []
    feedback = ""
    for dix, dname in enumerate(docnames):
        if dix in train:
            item_grade = str([tritems[ix][1] for ix, trix in enumerate(train) if int(trix) == dix][0])
            grades.append(";".join([dname, item_grade, feedback]))
        else:
            item_grade = str([results[ix] for ix, tsix in enumerate(test) if int(tsix) == dix][0])
            grades.append(";".join([dname, item_grade, feedback]))

    DataIO().instance().save_document("resultado.csv", grades, pathto=dataset_directory)

    """

    Generate reports explaining the classifier/regressor grades

    - Lista de Respostas
    - Macros
    - Performance
    - Rubrics
    |-----------------------------------------|
    | Use 4-Bins to generate grade categories |
    | in continuous data                      |
    |-----------------------------------------|
    """
    grades_lst = []
    dbname = []

    report_output = output_directory+"report/"
    DataIO().instance().output_check(report_output)

    if args.data_structure == "single_file":
        dbname = args.dataset.split("/")[-2]+" : "+args.data_file
    else:
        dbname = args.dataset.split("/")[-2]

    report_mod = Report(dbname, students, responses, report_output)

    if args.grade_model == "continuous":
        discr = KBinsDiscretizer(n_bins=4, encode='ordinal', strategy='uniform')
        grades_lst = discr.fit_transform(np.array([float(g.split(";")[1]) for g in grades]).reshape(-1, 1))
        grades_lst = ["Classe-"+str(g_[0]) for g_ in grades_lst]
    else:
        grades_lst = [g.split(";")[1].replace("_","-") for g in grades]

    report_mod.report_tex_lista_respostas(grades, enable_grades=True)

    if os.path.isfile(dataset_directory+"labels"):
        performance = report_mod.report_tex_performance(clf=std_clf.lower(), grademod=args.grade_model, datasetdir=dataset_directory, outputdir=output_directory)
        logger.info(performance)

    report_mod.report_tex_macros()

    if args.individual_reports == "enabled":
        report_mod.report_tex_rubrics(grades_lst, language=args.language, indiv_feedbacks=True)
    else:
        report_mod.report_tex_rubrics(grades_lst, language=args.language, indiv_feedbacks=False)

    logger.info("[pNota @ "+str(socket.gethostname())+"] End: [Method Classification] Done!")

if __name__ == "__main__":
    main()
