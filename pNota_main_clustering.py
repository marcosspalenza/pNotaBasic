"""
pNota 2021.
Author: Marcos A. Spalenza

"""
# native and third-part libraries
import os
import sys
import time
import socket
import logging # outputmanager requisite
import resource # outputmanager requisite
import numpy as np
from datetime import timedelta # outputmanager requisite
from argparse import ArgumentParser # arguments manager

# pNota files
from pNota_report import Report
from pNota_cluster_info import Clustering
from pNota_preprocessing import Preprocessing, DataIO, Vectorization, Dataset

'''
Start Methods

Global Variables
'''
LOG_FILENAME = "pNota.log"

def transform_documents(tokens, processor, method="stemm"):
    # interframework compatibility module : spacy // nltk
    if method == "lemma":
        # spaCy
        documents = [" ".join(tk) for tk in tokens]
        docs_anlz = processor.analyze(documents, method="LEMMA")
        documents = [" ".join(tks) for tks in docs_anlz]

    elif method == "stemm":
        # NLTK
        tokens = processor.tranform_txt(tokens, method="snowball")
        documents = [" ".join(tk) for tk in tokens]

    else:
        # No process
        documents = [" ".join(tk) for tk in tokens]

    return documents

'''
Main Method
'''
def main():
    '''
    args Parser for first step processing
    '''
    parser = ArgumentParser(usage="%(prog)s [args] <datasetdir>",  description="pNota: A Short Answer Scoring System, Sampling by Clustering Analysis")
    parser.add_argument("dataset", help="Dataset folder.")
    parser.add_argument("-s", "--dbstructure", dest="data_structure", default="moodle_files", help="Dataset input structure. Default: moodle_files. [ moodle_files, single_file, just_files ]")
    parser.add_argument("-f", "--filename", dest="data_file", default="resposta.txt", help="Input file name. Default resposta.txt.")
    parser.add_argument("-d", "--trdoc", dest="train_document", default="notastreino.csv", help="Data train/test document. Default: notastreino.csv.")
    parser.add_argument("-e", "--encoding", dest="encoding", default="utf-8", help="Dataset encoding for all files. Default: utf-8. [ utf-8, iso-8859-1 ]")
    parser.add_argument("-k", "--nclusters", dest="n_clusters", default="0", help="Number of clusters. Default: 0 (use optimization).")
    parser.add_argument("-t", "--trmethod", dest="train_method", default="30", help="Train percentual in data selection method. Default 30.")
    parser.add_argument("-l", "--language", dest="language", default="portuguese", help="Dataset language. Default: portuguese. [ portuguese, english, spanish ]")
    parser.add_argument("-c", "--container", dest="enable_container", default="enabled", help="Enable clustering containers. Default: enabled. [ enabled, disabled ]")
    parser.add_argument("-w", "--wordmod", dest="enable_WordSTD", default="stemm", help="Enable textual analysis by Stemming or Lemmatization methods. Default: stemm. [ stemm, lemma (Not recommended), none ]")
    parser.add_argument("-p", "--pos", dest="enable_POS", default="enabled", help="Enable textual analysis using Part-Of-Speech tags. Default: enabled. [ enabled, disabled ]")
    parser.add_argument("-n", "--ner", dest="enable_NER", default="enabled", help="Enable textual analysis by Named Entity Recognition methods. Default: enabled. [ enabled, disabled ]")
    parser.add_argument("-m", "--morph", dest="enable_MORPH", default="enabled", help="Enable textual analysis using Morphological tags. Default: enabled. [ enabled, disabled ]")
    parser.add_argument("-i", "--ignore_clusters", dest="ignore_clusters", default="disabled", help="Ignore existing clusters and create new clustering results. Default: disabled. [ enabled, disabled ]. Not recommended, remove previous train selection.")
    parser.add_argument("-r", "--generate_reports", dest="generate_reports", default="enabled", help="Generate dataset reports. Default: enabled. [ enabled, disabled ]")
    parser.add_argument("-x", "--ngram_start", dest="ngram_start", default="1", help="Starting n-gram range values for indexing. Default 1.")
    parser.add_argument("-y", "--ngram_end", dest="ngram_end", default="5", help="Ending n-gram range values for indexing. Default 1.")
    args = parser.parse_args()

    '''
    Args Verification
    '''
    dataset_directory = args.dataset
    output_directory = dataset_directory+"/logs_pNota/"
    DataIO().instance().output_check(output_directory)

    print("- INPUT \t : "+dataset_directory)
    print("- OUTPUT \t : "+output_directory)
    print(args)

    '''
    Output Manager 
        Add a custom formatter to root logger for output information
    '''
    logging.basicConfig(filename=output_directory+LOG_FILENAME, format='%(asctime)s \t %(message)s', datefmt='%d/%m/%Y %I:%M:%S', level=logging.DEBUG)
    handler = logging.StreamHandler()
    logging.getLogger().addHandler(handler)
    logger = logging.getLogger()

    try:
        if os.stat(output_directory+LOG_FILENAME).st_ctime < time.time() - 7 * 86400:
            os.remove(output_directory+LOG_FILENAME)
            with open(output_directory+LOG_FILENAME, "a") as wtr:
                wtr.write("pNota @ "+str(socket.gethostname())+"\n\n")
    except:
        logger.info("[Warning] File [run.log] not found! A new [run.log] was automatically created.")

    logger.info("[Info] \t Std input : "+dataset_directory)
    logger.info("[Info] \t Std output : "+output_directory)
    logger.info("[Info] \t Language : "+str(args.language).upper())
    logger.info("[Info] \t Dataset format : "+str(args.data_structure).upper())
    logger.info("[Info] \t Dataset file : "+str(args.data_file).upper())
    logger.info("[Info] \t Standard encoding : "+str(args.encoding).upper())
    logger.info("[Info] \t Class information : "+str(args.train_document).upper())

    '''
    Data IO
    '''
    logger.info("[pNota @ "+str(socket.gethostname())+"] Started: Method [Clustering] for "+[d for d in dataset_directory.split('/') if d != ""][-1])

    data_methods = Dataset(args.data_structure, language_=args.language, encoding_=args.encoding)
    data = data_methods.get_dataset(dataset_directory, output_directory, filename=args.data_file)


    if data == []:
        logger.error("[Error] \t Dataset empty.")
        sys.exit()

    '''
    Pre Processing
    '''
    data_methods = Preprocessing(language_=args.language)

    students, responses = zip(*data)

    if len(students) < 10:
        logger.error("[Info] \t Waiting complete dataset. Document size < 10.")
        sys.exit()

    logger.info("[Process] \t Reading "+str(len(students))+" document items")

    logger.info("[Process] \t Filtering valuable data")
    documents = data_methods.standardize_txt(responses, methods=["accents", "punct", "spaces", "tags", "alphanumeric"])

    pos_vec = [] # vector containing Part-of-Speech Tags usings first stage documents
    ner_vec = [] # vector containing Named Entity Recognition tags using first stage documents
    mph_vec = [] # vector containing Morphological tags using first stage documents

    if args.enable_POS == "enabled":
        logger.info("[Process] \t POS-Tag")
        pos_vec = data_methods.analyze(documents, method="POS")

    if args.enable_MORPH == "enabled":
        logger.info("[Process] \t Morph")
        mph_vec = data_methods.analyze(documents, method="MORPH")

    if args.enable_NER == "enabled":
        logger.info("[Process] \t NER")
        ner_vec = data_methods.analyze(documents, method="NER")

    tokens = data_methods.tokenize_txt(documents)
    tokens = data_methods.filter_txt(tokens, methods=["stopwords", "smallwords", "largewords"])

    if args.enable_WordSTD == "stemm":
        logger.info("[Process] \t Stemming")

    elif args.enable_WordSTD == "lemma":
        logger.info("[Process] \t Lemmatization")

    documents = transform_documents(tokens, data_methods, method=args.enable_WordSTD)

    # Type: lowercase
    logger.info("[Process] \t Case: lower")
    docs_anlz = data_methods.analyze(documents, method="lowercase")
    documents = [" ".join(tks) for ti, tks in enumerate(docs_anlz)]

    """
    # Type: UPPERCASE
    logger.info("[Process] \t Case: UPPER")
    docs_anlz = data_methods.analyze(documents, method="UPPERCASE")
    documents = [" ".join(tks)+" "+documents[ti] for ti, tks in enumerate(docs_anlz)]
    """

    if pos_vec != []:
        documents = [documents[ti]+" "+" ".join(tks) for ti, tks in enumerate(pos_vec)]
    if ner_vec != []:
        documents = [documents[ti]+" "+" ".join(tks) for ti, tks in enumerate(ner_vec)]
    if mph_vec != []:
        documents = [documents[ti]+" "+" ".join(tks) for ti, tks in enumerate(mph_vec)]

    print(documents[1])
    logger.info("[Process] \t Document vectorization")
    vectorizer = Vectorization(documents, encoding_=args.encoding)

    logger.info("[Process] \t Generating a Term Frequency [TF Vectorization]")
    logger.info("[Process] \t TF Vectorization N-Gram Range [ "+str(args.ngram_start)+" : "+str(args.ngram_end)+" ].")
    vectors, features = vectorizer.tfidf(ngram_start=int(args.ngram_start), ngram_end=int(args.ngram_end))

    feat_count = vectors.sum(axis=0)
    features = [str(features[vix])+"\t"+str(round(feat_count[0, vix], 2)) for vix in range(np.shape(vectors)[1])]

    DataIO().instance().save_document("wordlist.txt", features, pathto=output_directory)
    '''
    Clustering 
    '''
    logger.info("[Process] \t Clustering")
    clstr = Clustering(vectors, dataset_directory, output_directory)

    clstrix = []
    cluster_labels = []

    mnt_clusters = args.ignore_clusters == "disabled"

    if args.enable_container == "enabled":
        logger.info("[Process] \t Module CLSTR - Container")
        clstrix, cluster_labels = clstr.run_clustering(module="docker", mount_clusters=mnt_clusters)
    else:
        logger.info("[Process] \t Module CLSTR - Local")
        clstrix, cluster_labels = clstr.run_clustering(module="clstr", mount_clusters=mnt_clusters)

    logger.info("[Info] \t Dataset splitted between "+str(len(np.unique(cluster_labels)))+" clusters")
    logger.info("[Info] \t Clusters [Id./ Size] "+str([(u, len([c for c in cluster_labels if c == u])) for u in np.unique(cluster_labels)]))

    '''
    Train / Test Selection
    '''
    train_part, test_part = clstr.train_test_split(cluster_labels, train_mode=int(args.train_method))
    logger.info("[Info] \t Train size: "+str(len(train_part))+" \t\t Test size: "+str(len(test_part)))

    DataIO().instance().save_document("train", [str(ix) for ix in train_part], pathto=output_directory)
    DataIO().instance().save_document("test", [str(ix) for ix in test_part], pathto=output_directory)

    train_size = len(train_part)
    logger.info("[Info] \t "+str(train_size)+" ["+str(train_size*100 / len(students))+" %] documents requested in train set")
    
    train_file = []
    for sid, s in enumerate(students):
        if sid in train_part:
            train_file.append([str(s), "-1.0", "#"])
        else:
            train_file.append([str(s), "-1.0", ""])

    train_file = [";".join(t) for t in train_file]

    DataIO().instance().save_document("notastreino.csv", train_file, pathto=dataset_directory)

    if args.generate_reports == "enabled":        
        dbname = []
        report_output = output_directory+"report/"
        DataIO().instance().output_check(report_output)

        if args.data_structure == "single_file":
            dbname = args.dataset.split("/")[-2]+" : "+args.data_file
        else:
            dbname = args.dataset.split("/")[-2]

        report_mod = Report(dbname, students, responses, report_output)

        report_mod.report_tex_macros()

        report_mod.report_tex_clusters_distribution(cluster_labels, train_part)

        report_mod.report_tex_train(train_part, test_part)

        report_mod.report_tex_features()

        report_mod.report_tex_lista_respostas(train_file, enable_grades=False)

        report_mod.report_tex_lista_treino(outputdir=output_directory)

    logger.info("[pNota @ "+str(socket.gethostname())+"] End: Method [Clustering]")

if __name__ == "__main__":
    main()