"""
pNota 2021.
Author: Marcos A. Spalenza

"""
import os
import re
import sys
import csv
import unidecode
import unicodedata
import scipy as sp
import numpy as np
import pandas as pd
import random as rdm
import pt_core_news_sm # spaCy model
import en_core_web_sm # spaCy model
import xx_ent_wiki_sm # spaCy model
import es_core_news_sm # spaCy model
from html2text import html2text
# from gensim.models import Word2Vec
# from gensim.models.phrases import Phraser, Phrases
# from gensim.models import KeyedVectors
from nltk import stem, tokenize, corpus
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

class DataIO:
    '''
    Singleton
    Code >>> https://stackoverflow.com/questions/31875/is-there-a-simple-elegant-way-to-define-singletons
    '''
    def __init__(self, encoding_="utf-8"):
        self.dataio = ControlIO(encoding_)
    def instance(self):
        return self.dataio

class ControlIO:

    def __init__(self, encoding_="utf-8"):
        self.encoding = encoding_
    
    '''
    output folder check
    '''
    def output_check(self, folder):
        try:
            os.stat(folder)
        except:
            os.makedirs(folder)

    '''return entire content of the document in path as a string'''
    def read_document(self, filename, pathto="./"):
        with open(pathto+filename,"r") as txt:
            return txt.read()

    '''filedata is a data raw with multiple lines to be inserted like a matrix'''
    def save_document(self, filename, filedata, pathto="./"):
        with open(pathto+filename,"w", encoding=self.encoding) as txt:
            return txt.write("\n".join(filedata))

    '''read data in csv document and return like a matrix or header vector and matrix'''
    def read_csv_document(self, filename, pathto="./", header=True, std_separator='\t'):
        with open(pathto+filename) as csvready:
            filereader = csv.reader(csvready, delimiter=std_separator)
            data = []
            for row in filereader:
                data.append('\t'.join(row))
            if header:
                return data[0], data[1:]
            else:
                return data
    
    def save_csv_document(self, filename, data, pathto="./", delimiter_=';'):
        with open(pathto+filename, 'w', newline='\n') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=delimiter_, quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for r in data:
                csvwriter.writerow(r)

    def save_matrix(self, filename, data, pathto="./", fmt="%.4g", delimiter=' '):
        with open(pathto+filename, 'w') as fh:
            fh.write(str(np.shape(data)[0])+" "+str(np.shape(data)[1])+"\n")
            for rid in range(np.shape(data)[0]):
                lista = delimiter.join("0" if data[rid,idx] == 0 else fmt % data[rid,idx] for idx in  range(np.shape(data[rid])[1]))
                fh.write(lista + '\n')

    def save_csr_matrix(self, filename, data, pathto="./", fmt=4, delimiter=' '):
        mm_values = []
        sum_ = 0
        for docid in range(np.shape(data)[0]):
            line = data[docid, :].todense()
            for enum in range(np.shape(data)[1]):
                l = line[:, enum].item() # transform matrix[0][0] item to value
                if float(l) != 0.0:
                    mm_values.append(str(docid+1)+" "+str(enum+1)+" "+str(round(l, fmt)))
                    sum_ += 1
        with open(pathto+filename, 'w') as arq:
            # arq.write("% Matrix Market\n")
            arq.write(str(np.shape(data)[0])+" "+str(np.shape(data)[1])+" "+str(sum_)+"\n")
            for m in mm_values:
                arq.write(str(m)+"\n")

    def read_matrix(self, filename,  pathto="./", fsep=" ", header=1):
        data = []
        with open(pathto+filename, 'r') as fh:
            head = []
            while header > 0:
                head.append(fh.readline())
                header = header - 1
            docs = fh.read()
            try:
                data = np.array([[float(l) for l in line.split(fsep)] for line in docs.split("\n") if line != ""])
            except Exception as err:
                print("[Error] Data I/O problems. "+str(err))
        return data

    def read_csr_matrix(self, filename, pathto="./", fsep=" ", header=1):
        data = []
        with open(pathto+filename, 'r') as fh:
            head = []
            while header > 0:
                head.append(fh.readline())
                header = header - 1
            docid = head[-1]
            docs = fh.read()
            try:
                data = np.zeros((int(docid.split(fsep)[0]), int(docid.split(fsep)[1])))
                for n in docs.split("\n"):
                    if n != "":
                        id1, id2, n = n.split(fsep)
                        data[int(id1)-1, int(id2)-1] = float(n)
            except Exception as err:
                print("[Error] Data I/O problems. "+str(err))
        return data

class Dataset():
    TO_IGNORE = ["clusters", "logs_pNota", "logs", "labels", "notastreino.csv", "notas.csv", "resultado.csv", "intervalonotas.csv"]

    ANSWERS = "Resposta"
    STUDENT_ID = "Id"

    def __init__(self, datasource_, language_="portuguese", encoding_="utf-8"):
        self.datasource = datasource_ # moodle_files, single_file or just_files
        self.encoding = encoding_
        self.language = language_

    def get_dataset(self, pathto, output, filename="resposta.txt", header=True, delimiter=";"):
        data = []

        if self.datasource == "moodle_files":
            lst_stud = []
            lst_subm = []

            local_files = [x for x in os.listdir(pathto) if x not in self.TO_IGNORE]
            student_files = [os.path.join(pathto,subdir) for subdir in local_files if os.path.isdir(os.path.join(pathto,subdir))]

            lstdocuments = [(docid.split("/")[-1]) for docid in student_files]

            ord_doc = [re.sub("[^0-9]", "", docid) for docid in lstdocuments]
            if len(list(set(ord_doc))) == len(lstdocuments) and "" not in ord_doc:
                # mixed student index
                ord_doc = [int(re.sub("[^0-9]", "", docid)) for docid in lstdocuments]
            else:
                # string student index
                ord_doc = [docid.strip() for x in lstdocuments]

            lstdocuments = [lstdocuments[ix[0]] for ix in sorted(enumerate(ord_doc), key=lambda x:x[1])]

            for subdir in lstdocuments:
                with open(pathto+"/"+subdir+"/"+filename,"r", encoding=self.encoding) as txt:
                    student = subdir[(subdir.rfind("/")+1): ]
                    submission = html2text(txt.read(), self.encoding)
                    if submission.strip() != "":
                        lst_stud.append(student)
                        lst_subm.append(submission)

            data = [d for d in zip(lst_stud, lst_subm)]
            DataIO().instance().save_document("doclist.mtx", [str(folder_) for folder_, sub_ in data], output)

        elif self.datasource == "single_file":
            if ".csv" in filename or ".tsv" in filename:
                if header:
                    data = pd.read_csv(pathto+filename, sep=delimiter, encoding=self.encoding)
                else:
                    data = pd.read_csv(pathto+filename, sep=delimiter, encoding=self.encoding, header=None)

                data = list(zip([str(d_) for d_ in data[self.STUDENT_ID].values], [str(d_) for d_ in data[self.ANSWERS].values]))
                DataIO().instance().save_document("doclist.mtx", [str(folder_) for folder_, sub_ in data], output)


            elif ".xlsx" in filename or ".ods" in filename:
                if header:
                    data = pd.read_excel(pathto+filename, encoding=self.encoding)
                else:
                    data = pd.read_excel(pathto+filename, encoding=self.encoding, header=None)

                data = list(zip([str(d_) for d_ in data[self.STUDENT_ID].values],[str(d_) for d_ in data[self.ANSWERS].values]))
                DataIO().instance().save_document("doclist.mtx", [str(folder_) for folder_, sub_ in data], output)

            else:
                data = DataIO().instance().read_document(filename, pathto=pathto)
                data = list(zip([str(ix) for ix, d in enumerate(data.split("\n")) if d != ""], [str(d) for d in data.split("\n") if d != ""]))
                DataIO().instance().save_document("doclist.mtx", [str(folder_) for folder_, sub_ in enumerate(data)], output)

        elif self.datasource == "just_files":
            lst_stud = []
            lst_subm = []

            just_files = [f for f in os.listdir(pathto) if os.path.isfile(os.path.join(pathto,f))]
            just_files = [f for f in just_files if f not in self.TO_IGNORE]

            for f in just_files:
                doc = html2text(DataIO().instance().read_document(f, pathto=pathto))
                lst_stud.append(f[:-4])
                if "." in doc:
                    lst_subm.append(doc.split(".")[0])
                else:
                    lst_subm.append(doc)
            data = list(zip(lst_stud, lst_subm))
            DataIO().instance().save_document("doclist.mtx", [str(folder_) for folder_, sub_ in data], output)

        return data

class Preprocessing:

    STD_MTH = ["accents", "punct", "spaces", "tags", "alphanumeric"]
    TKN_MTH = ["simple", "word", "regex", "informal"]
    FLT_MTH = ["smallwords", "stopwords", "largewords", "numerals"]
    ANZ_MTH = ["NER", "POS", "MORPH", "LEMMA", "lowercase", "UPPERCASE"]
    TRF_MTH = ["wordnet", "snowball", "rslp", "lancaster", "porter"]

    def __init__(self, language_="portuguese"):
        """
        Languages
        * portuguese
        * english
        * spanish
        """
        self.documents = []
        self.language = language_


    """

    STANDARDIZE

    """
    def standardize_txt(self, documents_, methods=["accents", "punct", "spaces", "tags", "alphanumeric"]):
        """
        [stdz] accents
        [stdz] punct
        [stdz] multiple spaces
        [stdz] tags
        [stdz] alphanumeric

        MODULES:
        __standard_lowercase(self, text)
        __standard_uppercase(self, text)
        __standard_accents(self, text)
        __standard_punct(self, text)
        __standard_html(self, text)
        __standard_spaces(self, text)
        """
        self.documents = documents_

        unavaliable = [mth for mth in methods if mth not in self.STD_MTH]
        
        assert unavaliable == [], "Calling unavaliable methods inside the package "+str(unavaliable)

        for mth in methods:
            if mth == "accents":
                self.documents = [self.__standard_accents(d) for d in self.documents]
            elif mth == "punct":
                self.documents = [self.__standard_punct(d) for d in self.documents]
            elif mth == "spaces":
                self.documents = [self.__standard_spaces(d) for d in self.documents]
            elif mth == "tags":
                self.documents = [self.__standard_tags(d) for d in self.documents]
            elif mth == "alphanumeric":
                self.documents = [self.__standard_alphanumeric(d) for d in self.documents]

        return self.documents

    def __standard_alphanumeric(self, text):
        return ''.join([ch if ch == " " or ch.isalnum() else "" for ch in text])

    def __standard_accents(self, text):
        std_text = unidecode.unidecode(text)
        # std_text = unicodedata.normalize('NFD', text) # remove letter accents
        return u''.join(ch for ch in std_text if unicodedata.category(ch) != 'Mn')

    def __standard_punct(self, text):
        return re.sub("\W+", " ", text) # remove punctuation

    def __standard_tags(self, text):
        return re.sub("<.*?>", "", text) # remove tags and inside content
    
    def __standard_spaces(self, text):
        return re.sub(" \s", "", text) # remove multiple spaces, keeps single ones. Includes [ \t\n\r\f\v].

    """

    TOKENIZE

    """
    def tokenize_txt(self, documents_, method="word"):
        """
        [tokn] simple
        [tokn] word
        [tokn] regex
        [tokn] informal

        MODULES:
        __tokenize_simple(self, text)
        __tokenize_word(self, text)
        __tokenize_regex(self, text)
        __tokenize_informal(self, text)

        References:
        *** http://www.nltk.org/api/nltk.tokenize.html#module-nltk.tokenize.casual
        *** http://www.nltk.org/api/nltk.tokenize.html#module-nltk.tokenize.punkt
        *** http://www.nltk.org/api/nltk.tokenize.html#module-nltk.tokenize.regexp
        *** http://www.nltk.org/api/nltk.tokenize.html#module-nltk.tokenize.simple
        *** http://www.nltk.org/api/nltk.tokenize.html#module-nltk.tokenize.repp
        *** http://www.nltk.org/api/nltk.tokenize.html#module-nltk.tokenize.texttiling
        """
        self.documents = documents_

        assert method in self.TKN_MTH, "Calling unavaliable methods inside the package "+str(method)

        if method == "simple":
            self.documents = [d for d in self.__tokenize_simple()]
        elif method == "word":
            self.documents = [d for d in self.__tokenize_word()]
        elif method == "regex":
            self.documents = [d for d in self.__tokenize_regex()]
        elif method == "informal":
            self.documents = [d for d in self.__tokenize_informal()]

        return self.documents

    def __tokenize_simple(self):
        for d in self.documents:
            yield text.split(d)

    def __tokenize_word(self):
        for d in self.documents:
            yield tokenize.word_tokenize(d)

    def __tokenize_regex(self, rgx='\w+'):
        tknizer = tokenize.RegexpTokenizer(rgx)
        for d in self.documents:
            yield tknizer.tokenize(d)

    def __tokenize_informal(self):
        tknizer = tokenize.TweetTokenizer()
        for d in self.documents:
            yield tknizer.tokenize(d)
    """

    FILTER

    """
    def filter_txt(self, documents_, methods=["smallwords", "stopwords", "largewords", "numerals"]):
        """
        [fltr] stopwords
        [fltr] smallwords
        [fltr] numbers
        [fltr] largewords

        MODULES:
        __removal_stopword(self, doc)
        __removal_smallwords(self, doc)
        __removal_numerals(self, doc)
        __removal_largewords(self, doc)
        """
        self.documents = documents_

        unavaliable = [mth for mth in methods if mth not in self.FLT_MTH]

        assert unavaliable == [], "Calling unavaliable methods inside the package "+str(unavaliable)

        for mth in methods:
            if mth == "smallwords":
                self.documents = [self.__removal_smallwords(d) for d in self.documents ]
            elif mth == "stopwords":
                self.documents = [self.__removal_stopword(d) for d in self.documents ]
            elif mth == "largewords":
                self.documents = [self.__removal_largewords(d) for d in self.documents ]
            elif mth == "numerals":
                self.documents = [self.__removal_numerals(d) for d in self.documents ]

        return self.documents

    def __removal_smallwords(self, tokens, tksize=3):
        return [t for t in tokens if len(t) > tksize]

    def __removal_stopword(self, tokens):
        return [t for t in tokens if t.lower() not in corpus.stopwords.words(self.language)]

    def __removal_largewords(self, tokens, tksize=15):
        return [t for t in tokens if len(t) < tksize]

    def __removal_numerals(self, tokens):
        return [re.sub("[0-9]", "", t) for t in tokens]  # remove numerals
    
    """

    TRANSFORMATION

    """
    def tranform_txt(self, documents_, method="snowball"):
        """
        [stmm] snowball - for Arabic, Danish, Dutch, English, Finnish, French, German, Hungarian, Italian, Norwegian, Portuguese, Romanian, Russian, Spanish or Swedish
        [stmm] rslp - unique for Portuguese
        [stmm] lancaster - unique for English
        [stmm] porter - unique for English

        [lemm] wordnet - English

        MODULES: 
        __transform_snowball_stmm(self, tkns)
        __transform_lancaster_stmm(self, tkns)
        __transform_porter_stmm(self, tkns)
        __transform_rslp_stmm(self, tkns)
        __transform_wordnet_lemm(self, tkns)

        References:
        *** http://www.nltk.org/api/nltk.stem.html#module-nltk.stem.wordnet
        *** http://www.nltk.org/api/nltk.stem.html#module-nltk.stem.snowball
        *** http://www.nltk.org/api/nltk.stem.html#module-nltk.stem.rslp
        *** http://www.nltk.org/api/nltk.stem.html#module-nltk.stem.lancaster
        *** http://www.nltk.org/api/nltk.stem.html#module-nltk.stem.porter
        """
        self.documents = documents_

        assert method in self.TRF_MTH, "Calling unavaliable methods inside the package "+str(method)

        if method == "wordnet":
            self.documents = [self.__transform_wordnet_lemm(d) for d in self.documents]
        elif method == "snowball":
            self.documents = [self.__transform_snowball_stmm(d) for d in self.documents]
        elif method == "rslp":
            self.documents = [self.__transform_rslp_stmm(d) for d in self.documents]
        elif method == "lancaster":
            self.documents = [self.__transform_lancaster_stmm(d) for d in self.documents]
        elif method == "porter":
            self.documents = [self.__transform_snowball_stmm(d) for d in self.documents]
        return self.documents

    def __transform_snowball_stmm(self, tokens):
        stmm = stem.SnowballStemmer(self.language)
        return [stmm.stem(t) for t in tokens]

    def __transform_lancaster_stmm(self, tokens):
        stmm = stem.LancasterStemmer()
        return [stmm.stem(t) for t in tokens]

    def __transform_porter_stmm(self, tokens):
        stmm = stem.PorterStemmer()
        return [stmm.stem(t) for t in tokens]

    def __transform_rslp_stmm(self, tokens):
        stmm = stem.RSLPStemmer()
        return [stmm.stem(t) for t in tokens] 

    def __transform_wordnet_lemm(self, tokens):
        lemm = stem.WordNetLemmatizer()
        return [lemm.stem(t) for t in tokens]

    """

    ANALYZE

    """
    def analyze(self, documents_, method="lowercase"):
        """
        [anlz] lowercase
        [anlz] UPPERCASE
        [anlz] POS-TAG
        [anlz] MORPH
        [anlz] LEMMA
        [anlz] NER

        Regex
        *** Not implemented yet

        Collocations
        *** Not implemented yet
        """

        self.documents = documents_

        assert method in self.ANZ_MTH, "Calling unavaliable methods inside the package "+str(method)

        if method == "lowercase":
            return [[tk.lower() for tk in doc.split()] for doc in self.documents]

        elif method == "UPPERCASE":
            return [[tk.upper() for tk in doc.split()] for doc in self.documents]

        elif method == "NER":
            nlp = self.__load_model()
            return [[str(tk.ent_type_) if str(tk.ent_type_) != "" else "O" for tk in nlp(doc)] for doc in self.documents]

        elif method == "MORPH":
            nlp = self.__load_model()
            return [[" ".join([t_.split("=")[1]  if str(tk.morph) != "" else "O" for t_ in str(tk.morph).split("|")]) for tk in nlp(doc)] for doc in self.documents]

        elif method == "LEMMA":
            nlp = self.__load_model()
            return [[str(tk.lemma_) for tk in nlp(doc)] for doc in self.documents]

        elif method == "POS":
            nlp = self.__load_model()
            return [[str(tk.pos_) for tk in nlp(doc)] for doc in self.documents]

    def __load_model(self):
        if self.language == "portuguese":
            return pt_core_news_sm.load()

        elif self.language == "english":
            return en_core_web_sm.load()

        elif self.language == "spanish":
            return es_core_news_sm.load()

        else:
            # multi-language model
            return xx_ent_wiki_sm.load()

class Vectorization:
    ''' 
    Matrix frequencies, vectorizer, and weighting

    [vect] ngram
    [vect] word
    [vect] char
    [vect] TF
    [vect] TF-IDF
    [vect] wordlist

    References:
    *** http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.HashingVectorizer.html
    *** http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html
    *** http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html
    '''

    def __init__(self, text_array_, encoding_="utf-8"):
        self.text_array = text_array_
        self.encoding = encoding_


    def tf(self, decode_err_='ignore', ngram_start=1, ngram_end=1, mode_="word", binary_=False, wordlist_=None):
        assert ngram_start > 0 and ngram_end > 0, "N-gram range out of bounds"
        ngram = (ngram_start, ngram_end)
        vectorizer = CountVectorizer(ngram_range=ngram, vocabulary=wordlist_, encoding=self.encoding, decode_error=decode_err_, analyzer=mode_, binary=binary_)
        vectors = vectorizer.fit_transform(self.text_array)
        words = vectorizer.get_feature_names_out()
        return vectors, words


    def tfidf(self, decode_err_='ignore', ngram_start=1, ngram_end=1, mode_="word", binary_=False, wordlist_=None):
        assert ngram_start > 0 and ngram_end > 0, "N-gram range out of bounds"
        ngram = (ngram_start, ngram_end)
        vectorizer = TfidfVectorizer(ngram_range=ngram, vocabulary=wordlist_, encoding=self.encoding, decode_error=decode_err_, analyzer=mode_, binary=binary_)
        vectors = vectorizer.fit_transform(self.text_array)
        words = vectorizer.get_feature_names_out()
        return vectors, words


"""
class Embeddings:
    '''
    DOC TODO
    '''

    def portuguese_embeddings(self):
        model = KeyedVectors.load_word2vec_format("cbow_s1000.txt")

    def dataset_embeddings(self, text):
        sentences = [seq for seq in text]
        common_terms = ["of", "with", "without", "and", "or", "the", "a"]
        # Create the relevant phrases from the list of sentences:
        phrases = Phrases(sentences, common_terms=common_terms)
        # The Phraser object is used from now on to transform sentences
        bigram = Phraser(phrases)
        # Applying the Phraser to transform our sentences is simply
        sentences = list(bigram[sentences])
        model = Word2Vec(
            sentences, 
            min_count=3,   # Ignore words that appear less than this
            size=200,      # Dimensionality of word embeddings
            workers=2,     # Number of processors (parallelisation)
            window=5,      # Context window for words during training
            iter=30        # Number of epochs training over corpus
        )
"""
