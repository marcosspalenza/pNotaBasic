"""
pNota 2021.
Author: Marcos A. Spalenza

"""
# REPORT

# Lista Respostas
# Lista Treino
# Lista Clusters

"""
Relatórios:
- Relatório de Treino : 

Apresentação dos dados de acordo com os clusters e as opções de coleta.
Similaridade

Associação dos itens de resposta dentro do cluster / similaridade
Resultados

- Comparações humano x máquina

Valores de qualidade de classificação (desempenho) (Acc Prc Rec F1)
Características

- Principais características

Análise de distribuição de features

- Notas

Revisão dos resultados de treino por grupos de resposta nota / cluster / sim.
"""
import os
import re
import unidecode
import unicodedata
import numpy as np
import matplotlib as mpl
from itertools import product
from nltk.corpus import stopwords
from matplotlib import pyplot as plt
from sklearn.pipeline import make_pipeline
from lime.lime_text import LimeTextExplainer
from sklearn.tree import DecisionTreeClassifier, plot_tree
from sklearn.decomposition import LatentDirichletAllocation
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, \
    mean_squared_error, mean_absolute_error, max_error, confusion_matrix, cohen_kappa_score

from pNota_preprocessing import DataIO, Preprocessing, Vectorization

mpl.rcParams['figure.dpi'] = 300


class Report:

    def __init__(self, dbname_, docid_,  documents_, report_directory_):
        self.dbname = dbname_.replace('_', '-')
        self.docid = [self.__remove_latex_special_chars(d) for d in docid_]
        self.documents = [self.__latex_stdtext(d) for d in documents_]
        self.report_directory = report_directory_


    def __remove_latex_special_chars(self, txt):
        tokenizer = re.compile('\w+')
        strip_accents = unidecode.unidecode(txt)
        strip_special_chars = re.sub("[^\x00-\x7f]", " ", strip_accents)
        return ' '.join(filter(str.isalnum, tokenizer.findall(strip_special_chars)))


    def __latex_stdtext(self, document):
        std_text = self.__remove_latex_special_chars(document)
        std_text = unicodedata.normalize('NFD', std_text) # remove letter accents
        # return re.sub('[^a-zA-Z0-9 \n\.]', '', txt)
        return u''.join(ch for ch in std_text if unicodedata.category(ch) != 'Mn')


    def report_tex_macros(self):
        macros = [
            "% MACROS\n\n",
            "\\newcommand{\\dataset}{"+str(self.dbname)+"}\n",
        ]
        DataIO().instance().save_document("macros.tex", macros, pathto=self.report_directory)


    def report_tex_lista_respostas(self, lstgrades, enc="utf-8", enable_grades=True):
        table_tex = (
            "\\begin{center}  \n \
            \\begin{longtable}{p{5cm} p{3cm} p{12cm}} \\\\ \\hline \n \
            Id & Nota & Resposta \\\\ \\hline  \n \
            \\endhead  \n \
            \\hline  \n \
            \\multicolumn{3}{r}{Continua na pr{\\'o}xima p{\\'a}gina} \\\\  \n \
            \\endfoot  \n \
            \\hline \\hline  \n \
            \\multicolumn{3}{r}{{\\'U}ltima p{\\'a}gina} \\\\  \n \
            \\endlastfoot  \n"
        )
        content = ""
        for rid, response in enumerate(self.documents):
            text = ""
            docix = self.docid[rid]
            if enable_grades:
                if "#" in lstgrades[rid].split(";")[2]:
                    text=str(docix)+" & \\textbf{"+str(lstgrades[rid].split(";")[1])+"} & "+response+" \n"
                else:
                    text=str(docix)+" & "+str(lstgrades[rid].split(";")[1])+" & "+response+" \n"
            else:
                text=str(docix)+" & - & "+response+" \n"
            content = content+"\n"+text+" \\\\ \\cline{2-3}\n"        
        table_tex = table_tex + content
        table_tex = table_tex + "\n" + "\\end{longtable}  \n \\end{center}"
        DataIO().instance().save_document("lst-respostas.tex", [table_tex.replace("_", "-")], pathto=self.report_directory)


    def report_tex_lista_treino(self, enc="utf-8", outputdir="./"):
        table_tex = (
            "\\begin{center} \n \
            \\begin{longtable}{p{2cm} p{2cm} p{2cm} p{14cm}} \\\\ \\hline \n \
            Cluster & Id & M{\\'e}todo & Resposta \\\\ \\hline \n \
            \\endhead \n \
            \\hline \n \
            \\multicolumn{4}{r}{Continua na pr{\\'o}xima p{\\'a}gina} \\\\  \\hline \\hline \n \
            \\endfoot \n \
            \\hline \\hline \
            \\multicolumn{4}{r}{{\\'U}ltima p{\\'a}gina} \\\\ \\hline \\hline \n \
            \\endlastfoot \n"
        )
        sampling = DataIO().instance().read_document("minmax.mat", pathto=outputdir)
        # MINMAX [0] clstrix [1] method [2, 3] itemix [4] score
        sampling = [i.split(" ") for i in sampling.split("\n") if i != ""]
        text = []
        for clstrix, method, item1, item2, score in sorted(sampling, key = lambda x : int(x[0])):
            if int(item1) != int(item2):
                text.append(str(clstrix)+" & "+str(item1)+" & "+str(method)+" & "+self.documents[int(item1)]+" \\\\ \\cline{3-4}\n")
                text.append(str(clstrix)+" & "+str(item2)+" & "+str(method)+" & "+self.documents[int(item2)]+" \\\\ \\cline{3-4}\n")
            else:
                text.append(str(clstrix)+" & "+str(item1)+" & "+str(method)+" & "+self.documents[int(item1)]+" \\\\ \\cline{3-4}\n")
        table_tex = table_tex + "\n" + "".join(text)
        table_tex = table_tex + "\n" + "\\end{longtable}  \n \\end{center}"
        DataIO().instance().save_document("lst-amostras.tex", [table_tex.replace ("_", "-")], pathto=self.report_directory)


    def report_tex_clusters_distribution(self, clusters, train):
        table_tex = (
            "\\begin{center} \n \
            \\begin{longtable}{p{2cm} p{2cm} p{16cm}} \\\\ \\hline \n \
            Cluster & Tamanho & Itens \\\\ \\hline \n \
            \\endhead \n \
            \\hline \n \
            \\multicolumn{3}{r}{Continua na pr{\\'o}xima p{\\'a}gina} \\\\  \\hline \\hline \n \
            \\endfoot \n \
            \\hline \\hline \
            \\multicolumn{3}{r}{{\\'U}ltima p{\\'a}gina} \\\\  \\hline \\hline \n \
            \\endlastfoot \n"
        )
        distribution = []
        for c in np.unique(clusters):
            clstr = [cid for cid, c2 in enumerate(clusters) if c2 == c]
            distribution.append(
                str(c)+" & "
                + str(len(clstr))+" & "+" ".join([
                    str(c2) if c2 not in train else "\\textbf{"+str(c2)+"}" for c2 in clstr
                ])
                + " \\\\ \\cline{3-3} \n"
            )
        table_tex = table_tex + "\n".join(distribution)
        table_tex = table_tex + "\n" + "\\end{longtable}  \n \\end{center}"
        DataIO().instance().save_document("lst-clusters.tex", [table_tex], pathto=self.report_directory)


    def report_tex_train(self, train, test):
        dbsize = len(train)+len(test)
        trsize = len(train)
        tssize = len(test)
        dataset_info = [
            "\\begin{flushleft}",
            "\\begin{tabular}{|c c c c|} \\hline",
            "\\multicolumn{3}{|l}{Dataset} & Amostras\\\\ \n",
            "\\multicolumn{3}{|l}{"+str(self.dbname)+"} & "+str(dbsize)+"\\\\ \\hline \n",
            " Treino (Un.) & Treino (\\%)  & Teste (Un.) & Teste (\\%) \\\\ \\hline \n",
            " & ".join([
                str(trsize),
                str(round(trsize*100/dbsize, 2)),
                str(tssize),
                str(round(tssize*100/dbsize, 2))
            ])+" \\\\ \n",
            "\\hline \\hline",
            "\\end{tabular}",
            "\\end{flushleft}",
        ]
        DataIO().instance().save_document("total-train.tex", dataset_info, pathto=self.report_directory)


    def report_tex_features(self, select=25):
        vectorizer = Vectorization(self.documents)
        vectors, features = vectorizer.tf(ngram_start=1, ngram_end=1)
        feat_count = vectors.sum(axis=0)
        features = [str(features[vix])+"\t"+str(feat_count[0, vix]) for vix in range(np.shape(vectors)[1])]
        nchars = []
        nwords = []
        dbsize = len(self.docid)
        ftsize = len(features)
        for d in self.documents:
            nchars.append(len(d))
            nwords.append(len(d.split()))
        dataset_info = [
            "\\begin{flushleft}",
            "\\begin{tabular}{c c c c c c c } \\hline",
            "\\multicolumn{6}{l}{Dataset} & Amostras \\\\ \n",
            "\\multicolumn{6}{l}{"+str(self.dbname)+"} & "+str(dbsize)+" \\\\ \\hline \n",
            "& \\multicolumn{3}{c}{Palavras} & \\multicolumn{3}{c}{Caracteres} \\\\ \n",
            "Caracter{\\'i}sticas & Max. & Min. & M{\\'e}dia & Max. & Min. & M{\\'e}dia \\\\ \\cline{2-4} \\cline{5-7} \n",
            " & ".join([
                str(ftsize),
                str(max(nwords)),
                str(min(nwords)),
                str(round(np.mean(nwords), 2)),
                str(max(nchars)),
                str(min(nchars)), 
                str(round(np.mean(nchars),2))
            ])+" \\\\ \n",
            "\\hline \\hline",
            "\\end{tabular}",
            "\\end{flushleft}",
        ]
        DataIO().instance().save_document("total-features.tex", dataset_info, pathto=self.report_directory)
        featcount = [
            features[f_[0]]
            for f_ in sorted(enumerate(
                [int(f.split("\t")[1]) for f in features]
            ),
            key= lambda x :x[1])
        ]
        fmax = [
            "\\begin{flushleft}",
            "\\begin{tabular}{| c  c |} \\hline",
            "Palavra & Frequ{\\^e}ncia \\\\ \\hline \n",
        ]
        for f in reversed(featcount[-1*(select):]):
            wd_ = f.split("\t")[0].replace("_", "-")
            fr_ = f.split("\t")[1]
            fmax.append(wd_+" & "+fr_+" \\\\ \n")
        fmax.extend([
            "\\hline",
            "\\end{tabular}",
            "\\end{flushleft}"
        ])
        DataIO().instance().save_document("lst-maxfeat.tex", fmax, pathto=self.report_directory)
        fmin = [
            "\\begin{flushleft}",
            "\\begin{tabular}{| c  c |} \\hline",
            "Palavra & Frequ{\\^e}ncia \\\\ \\hline \n",
        ]
        for f in reversed(featcount[:select]):
            wd_ = f.split("\t")[0].replace("_", "-")
            fr_ = f.split("\t")[1]
            fmin.append(wd_+" & "+fr_+" \\\\ \n")
        fmin.extend([
            "\\hline",
            "\\end{tabular}",
            "\\end{flushleft}",
        ])
        DataIO().instance().save_document("lst-minfeat.tex", fmin, pathto=self.report_directory)


    def report_tex_performance(self, clf="gbc", grademod="ordinal", datasetdir="./", outputdir="./"):
        trix = DataIO().instance().read_document("test", pathto=outputdir).split("\n")
        trix = [int(r) for r in trix if r != ""]
        labels = DataIO().instance().read_document("labels", pathto=datasetdir).split("\n")
        if grademod in ["continuous", "discrete"]:
            labels = [float(r) for r in labels[1:] if r != ""]
        else:
            labels = [r.replace("_","-") for r in labels[1:] if r != ""]
        testlbl = [labels[ix] for ix in trix]
        classifiers = []
        output = ["% Evaluate Results \n"]
        if grademod == "discrete" or grademod == "ordinal":
            files = ["svm.labels", "dtr.labels", "gbc.labels", "rdf.labels", "knn.labels", "wsd.labels"]
            for f in files:
                clflbl = DataIO().instance().read_document(f, pathto=outputdir).split("\n")
                if grademod == "discrete":
                    classifiers.append([float(r) for r in clflbl if r != ""])
                else:
                    classifiers.append([r for r in clflbl if r != ""])
            for l in range(len(classifiers)):
                results = [
                    files[l].split(".")[0].upper(),
                    "\t".join(["ACC", "PRE", "REC", "F1(m)", "F1(w)", "Kappa(ln)", "Kappa(qu)"]),
                    "\t".join([
                        str(accuracy_score(testlbl, classifiers[l])),
                        str(precision_score(testlbl, classifiers[l], average="macro")),
                        str(recall_score(testlbl, classifiers[l], average="macro")),
                        str(f1_score(testlbl, classifiers[l], average="macro")),
                        str(f1_score(testlbl, classifiers[l], average="weighted")),
                        str(cohen_kappa_score(testlbl, classifiers[l], weights="linear")),
                        str(cohen_kappa_score(testlbl, classifiers[l], weights="quadratic"))
                    ])
                ]
                DataIO().instance().save_document(files[l].split(".")[0].upper()+"-performance.txt", results, pathto=self.report_directory)
                output.extend([
                    "\\newpage",
                    "\n\n\\subsection*{"+files[l].split(".")[0].upper()+"}\n",
                    "\\begin{itemize} \n",
                ])
                results = [
                    "Accuracy: "+str(accuracy_score(testlbl, classifiers[l])),
                    "Precision: "+str(precision_score(testlbl, classifiers[l], average="macro")),
                    "Recall: "+str(recall_score(testlbl, classifiers[l], average="macro")),
                    "F1 (macro): "+str(f1_score(testlbl, classifiers[l], average="macro")),
                    "F1 (weighted): "+str(f1_score(testlbl, classifiers[l], average="weighted")),
                    "Cohen Kappa (linear): "+str(cohen_kappa_score(testlbl, classifiers[l], weights="linear")),
                    "Cohen Kappa (quadratic): "+str(cohen_kappa_score(testlbl, classifiers[l], weights="quadratic"))
                ]
                for r in results:
                    output.append("\\item "+r+"\n")
                output.append("\\end{itemize} \n")
                self.__plot_categ_performance__(testlbl, classifiers[l], filename="fig-eval"+files[l].split(".")[0].upper()+".png")
                self.__plot_confusion_matrix__(testlbl, classifiers[l], filename="fig-cm"+files[l].split(".")[0].upper()+".png")
                output.extend([
                    "\\begin{figure}[!h]",
                    "\\includegraphics[width=\\linewidth]{fig-eval"+files[l].split(".")[0].upper()+".png"+"}",
                    "\\end{figure}",
                    "\\newpage",
                    "\\begin{figure}[!h]",
                    "\\includegraphics[width=\\linewidth]{fig-cm"+files[l].split(".")[0].upper()+".png}",
                    "\\end{figure}",
                    "\\newpage",
                ])
            DataIO().instance().save_document("lst-clf.tex", output, pathto=self.report_directory)
            cix = [cix for cix, c_ in enumerate(files) if clf in c_.split(".")[0]][0]
            return (
                files[cix].split(".")[0]+"\n \
                Accuracy: "+str(accuracy_score(testlbl, classifiers[cix]))+"\n \
                Precision: "+str(precision_score(testlbl, classifiers[cix], average="macro"))+"\n \
                Recall: "+str(recall_score(testlbl, classifiers[cix], average="macro"))+"\n \
                F1 (macro): "+str(f1_score(testlbl, classifiers[cix], average="macro"))+"\n \
                F1 (weighted): "+str(f1_score(testlbl, classifiers[cix], average="weighted"))+"\n \
                Cohen Kappa (linear): "+str(cohen_kappa_score(testlbl, classifiers[cix], weights="linear"))+"\n \
                Cohen Kappa (quadratic): "+str(cohen_kappa_score(testlbl, classifiers[cix], weights="quadratic"))
            )
        else:
            files = ["linr.labels", "lssr.labels", "dtrg.labels", "knrg.labels", "wsrg.labels"]
            for f in files:
                clflbl = DataIO().instance().read_document(f, pathto=outputdir).split("\n")
                classifiers.append([float(r) for r in clflbl if r != ""])
            for l in range(len(classifiers)):
                results = [ 
                    files[l].split(".")[0].upper(),
                    "\t".join(["MAE", "MSE", "RMSE", "CorrP"]),
                    "\t".join([
                        str(mean_absolute_error(testlbl, classifiers[l])),
                        str(mean_squared_error(testlbl, classifiers[l])),
                        str(np.sqrt(mean_squared_error(testlbl, classifiers[l]))),
                        str(np.corrcoef(testlbl, classifiers[l])[0, 1])
                    ])
                ]
                DataIO().instance().save_document(files[l].split(".")[0].upper()+"-performance.txt", results, pathto=self.report_directory)
                output.append("\n\n \\subsection*{"+files[l].split(".")[0].upper()+"}\n")
                output.append("\\begin{itemize} \n")
                results = [
                    "MAE: "+str(mean_absolute_error(testlbl, classifiers[l])),
                    "MSE: "+str(mean_squared_error(testlbl, classifiers[l])),
                    "RMSE: "+str(np.sqrt(mean_squared_error(testlbl, classifiers[l]))),
                    "CorrP: "+str(np.corrcoef(testlbl, classifiers[l])[0, 1]),
                ]
                for r in results:
                    output.append("\\item "+r+"\n")
                output.append("\\end{itemize} \n")
                self.__plot_cont_performance__(testlbl, classifiers[l], filename="fig-eval"+files[l].split(".")[0].upper()+".png")
                self.__plot_regr_divergence__(testlbl, classifiers[l], filename="fig-err"+files[l].split(".")[0].upper()+".png")
                output.extend([
                    "\\begin{figure}[!h]",
                    "\\includegraphics[width=\\linewidth]{fig-err"+files[l].split(".")[0].upper()+".png"+"}",
                    "\\end{figure}",
                    "\\newpage",
                    "\\begin{figure}[!h]",
                    "\\includegraphics[width=\\linewidth]{fig-eval"+files[l].split(".")[0].upper()+".png"+"}",
                    "\\end{figure}",
                    "\\newpage",
                ])
            DataIO().instance().save_document("lst-clf.tex", output, pathto=self.report_directory)
            cix = [cix for cix, c_ in enumerate(files) if clf in c_][0]
            return (
                files[cix].split(".")[0]+"\n \
                MAE: "+str(mean_absolute_error(testlbl, classifiers[cix]))+"\n \
                MSE: "+str(mean_squared_error(testlbl, classifiers[cix]))+"\n \
                RMSE: "+str(np.sqrt(mean_squared_error(testlbl, classifiers[l])))+"\n \
                CorrP: "+str(np.corrcoef(testlbl, classifiers[cix])[0, 1])
            )

    def __plot_regression_divergence(self, truelbl, predlbl, filename="performance.png"):
        # Plot outputs
        plt.scatter(predlbl, truelbl, color='r')
        plt.plot(truelbl, truelbl, color='g', linewidth=3)
        plt.savefig(self.report_directory+filename)
        plt.clf()


    def __plot_categ_performance(self, truelbl, predlbl, filename="performance.png"):
        labels = ["ACC", "PRE", "REC", "F1"]
        evaluation = [
            accuracy_score(truelbl, predlbl)*100,
            precision_score(truelbl, predlbl, average="macro")*100,
            recall_score(truelbl, predlbl, average="macro")*100,
            f1_score(truelbl, predlbl, average="macro")*100
        ]
        x_distr = np.arange(len(labels))  # the label locations
        width = 0.35  # the width of the bars
        fig, ax = plt.subplots()
        bars = ax.bar(labels, evaluation, alpha=0.7, width=0.4)
        for ix, _ in enumerate(bars):
            if evaluation[ix] < 30:
                bars[ix].set_color('r')
            elif evaluation[ix] < 70:
                bars[ix].set_color('y')
            else:
                bars[ix].set_color('g')
        ax.plot([30 for _ in evaluation], color="r",  linestyle='--', linewidth=2.0, label="Insuficiente")
        ax.plot([50 for _ in evaluation], color="y", linestyle='--', linewidth=2.0, label="Adequado")
        ax.plot([70 for _ in evaluation], color="g", linestyle='--', linewidth=2.0, label="Avançado")
        ax.set_ylabel("Desempenho")
        ax.set_title(self.dbname)
        ax.set_xticks(x_distr)
        ax.set_xticklabels(labels)
        ax.set_ylim((0, 100))
        ax.legend(loc='lower right')
        fig.tight_layout()
        plt.savefig(self.report_directory+filename)
        plt.clf()


    def __plot_continuous_performance(self, truelbl, predlbl, filename="performance.png"):
        evaluation = [
            mean_absolute_error(truelbl, predlbl),
            mean_squared_error(truelbl, predlbl),
            np.sqrt(mean_squared_error(truelbl, predlbl)),
            np.corrcoef(truelbl, predlbl)[0, 1],
        ]
        labels = ['MAE', 'MSE', 'RMSE', 'CorrP']
        x_distr = np.arange(len(labels))  # the label locations
        width = 0.35  # the width of the bars
        fig, ax = plt.subplots()
        bars = ax.bar(labels, evaluation, alpha=0.7, width=0.4)
        ax.set_ylabel("Desempenho")
        ax.set_title(self.dbname)
        ax.set_xticks(x_distr)
        ax.set_xticklabels(labels)
        fig.tight_layout()
        plt.savefig(self.report_directory+filename)
        plt.clf()


    def __plot_confusion_matrix(self, testlbl, predlbl, filename="confusion_matrix.png"):
        conf_matrix = confusion_matrix(testlbl, predlbl, labels=np.unique(testlbl))
        fig, ax = plt.subplots()
        n_classes = conf_matrix.shape[0]
        im_ = ax.imshow(conf_matrix, interpolation='nearest', cmap='RdYlGn_r')
        text_ = None
        cmap_min, cmap_max = im_.cmap(0), im_.cmap(256)
        text_ = np.empty_like(conf_matrix, dtype=object)
        # print text with appropriate color depending on background
        thresh = (conf_matrix.max() + conf_matrix.min()) / 2.0
        for i, j in product(range(n_classes), range(n_classes)):
            color = cmap_max if conf_matrix[i, j] < thresh else cmap_min
            text_cm = format(conf_matrix[i, j], '.2g')
            if conf_matrix.dtype.kind != 'f':
                text_d = format(conf_matrix[i, j], 'd')
                if len(text_d) < len(text_cm):
                    text_cm = text_d
            text_[i, j] = ax.text(
                j, i, text_cm,
                ha="center", va="center",
                color=color)
        fig.colorbar(im_, ax=ax)
        ax.set_xticks(np.arange(n_classes))
        ax.set_yticks(np.arange(n_classes))
        ax.set_xticklabels(np.unique(testlbl), rotation = 90)
        ax.set_yticklabels(np.unique(testlbl))
        ax.set_xlabel("Predições")
        ax.set_ylabel("Notas")
        ax.set_ylim((n_classes - 0.5, -0.5))
        plt.tight_layout()
        plt.savefig(self.report_directory+filename)
        plt.clf()


    def __get_keywords(self, model, feature_names, n_top_words):
        for topic_idx, topic in enumerate(model.components_):
            yield " ".join([feature_names[i] for i in sorted(topic.argsort()[:-n_top_words - 1:-1])])


    def __generate_lime_class_report(self, clf_pipeline, grades, language_="portuguese"):
        table_tex = []
        categories = [lbl for lbl in np.unique(grades)]
        explainer = LimeTextExplainer(class_names=categories)
        docs = Preprocessing(language_=language_).standardize_txt(self.documents, methods=["accents"])
        for dix, doc in enumerate(docs):
            try:
                exp = explainer.explain_instance(doc, clf_pipeline.predict_proba, labels=[cix for cix, _ in enumerate(categories)])
                html_file = exp.as_html()
                DataIO().instance().save_document("document-"+str(dix)+".html", [html_file], pathto=self.report_directory)
                table_tex.extend([
                    "\\begin{itemize}",
                    "\\item Id:"+str(self.docid[dix]),
                    "\\item Classe: "+str(grades[dix]),
                    "\\item Feedback: document-"+str(dix)+".html",
                    "\\end{itemize} \n",
                ])
            except Exception as e:
                continue
        DataIO().instance().save_document("lst-feedback.tex", table_tex, pathto=self.report_directory)
        return table_tex


    def report_tex_rubrics(self, grades, n_responses=10, language="portuguese", individual_feedbacks=True):
        vectorizer = CountVectorizer() # apply simple vectorizer
        vectors = vectorizer.fit_transform(self.documents)
        features = vectorizer.get_feature_names_out()
        lda = LatentDirichletAllocation(n_components=1)
        table_tex = []
        topics = []
        for gr in np.unique(grades):
            grade_items = [ix for ix, gr_ in enumerate(grades) if gr_ == gr]
            lda.fit_transform(vectors[grade_items])
            words = " ".join(self.__get_keywords(lda, features, 5))
            topics.append(words)
            table_tex.extend([
                "\\begin{flushleft}",
                "\\begin{longtable}{ p{2cm} | p{12cm} } \\\\ \\hline",
                "\\multicolumn{2}{l}{\\textbf{"+str(self.dbname)+"}} \\\\ \\hline",
                "\\multicolumn{2}{c}{\\textbf{Nota: "+str(gr)+"}} \\\\ \\hline \n",
                "\\multicolumn{2}{l}{\\textit{T{\\'o}picos: "+str(words)+"}} \\\\ \\hline",
                " \\# & Exemplos \\\\ \\hline",
                "\\endhead  \\hline",
                "\\multicolumn{2}{r}{Continua na pr{\\'o}xima p{\\'a}gina} \\\\  \\hline \\hline",
                "\\endfoot",
                "\\multicolumn{2}{r}{{\\'U}ltima p{\\'a}gina} \\\\  \\hline \\hline",
                "\\endlastfoot",
            ])
            topic_rkg = [sum([1 if w in words.split(" ") else 0 for w in self.documents[ix].split(" ")]) for ix in grade_items]
            grade_items = [grade_items[ix[0]] for ix in sorted(enumerate(topic_rkg), key = lambda x:x[1], reverse=True)]
            grade_items = [ix for ix in sorted(grade_items[ : n_responses])]

            for ix in grade_items:
                table_tex.append(
                    str(ix)+" & "+" ".join(
                        ["\\textit{"+w+"}" if w in words.split(" ") else w
                            for w in self.documents[ix].split(" ")
                        ]
                    )+"\\\\ \\hline"
                )
            table_tex.extend([
                "\\hline",
                "\\end{longtable}",
                "\\end{flushleft} \n\n",
            ])
        DataIO().instance().save_document("lst-topics.tex", table_tex, pathto=self.report_directory)
        # simple classifier
        classifier = DecisionTreeClassifier(max_leaf_nodes=10)
        classifier.fit(vectors, grades)
        if individual_feedbacks == True:
            doc_pipeline = make_pipeline(vectorizer, classifier)
            self.__generate_lime_class_report(doc_pipeline, grades, language_=language)
        plt.figure(0)
        categories = [lbl for lbl in np.unique(grades)]
        plot_tree(classifier, feature_names=features, class_names=categories, filled=True)
        plt.tight_layout()
        plt.savefig(self.report_directory+"/fig-tree.png", quality=100)
        plt.clf() # Clear figure


"""
    def report_html_mapa_respostas(self):
        tokenizer = re.compile('\w+')
        wordset = []
        for rep in report:
            for word, grade, weight in rep:
                if wordset != [] or stemmWord(remover_acentuacao(word.lower())) not in [word_ for word_, value_, grade_ in wordset]:
                    wordset.append((stemmWord(word),weight, grade))
        print([word for word, value, grade in wordset])
        for idx, data in enumerate(student_data):
            folder, grade, answer = data
            text = ""
            for w in tokenizer.findall(answer):
                if stemmWord(remover_acentuacao(w.lower())) in [word_ for word_, value_, grade_ in wordset]:
                    index = [word_ for word_, value_, grade_ in wordset].index(stemmWord(remover_acentuacao(w.lower())))
                    intensity = int((1-round((wordset[index][1]*wordset[index][2])/2,1))*10)
                    if intensity > 9:
                        intensity = 9
                    text += "<b><font color='#"+(str(intensity)*6)+"'>"+w+" </font></b>"
                else:
                    text += w + " "
            html_str = "\
            <html>\n \
                <head>\n \
                    <meta charset="ISO-8859-1" />\n \
                </head>\n \
                <body>\n \
                    <p>"+text+"</p>\n \
                </body>\n \
            </html>"
            with open(mypath+folder+"/"+filename,"w", encoding='iso-8859-1') as html: 
                html.write(html_str)
        return wordset
"""
