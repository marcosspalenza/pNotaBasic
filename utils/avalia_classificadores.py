import sys
from argparse import ArgumentParser # arguments manager
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

def main():
    parser = ArgumentParser(usage="%(prog)s [args] <datasetdir>",  description="Evaluate the pNota classifiers.")
    parser.add_argument("dataset", help="Dataset folder.")

    args = parser.parse_args()

    files = ["svm.labels", "dtr.labels", "gbc.labels", "rdf.labels", "knn.labels", "wisard.labels"]

    print("[Process] Evaluate Classifiers' Results")
    print(args.dataset)

    classifiers = []
    labels = []
    trix = []

    with open(args.dataset+"logs_pNota/test", "r") as rdb:
        trix = [int(r) for r in rdb.read().split("\n") if r != ""]

    with open(args.dataset+"labels", "r") as rdb:
        labels = [r for r in rdb.read().split("\n") if r != ""][1:]

    testlbl = [labels[ix] for ix in trix]

    for f in files:
        with open(args.dataset+"logs_pNota/"+f, "r") as rdb:
            classifiers.append([r for r in rdb.read().split("\n") if r != ""])

    for l in range(len(classifiers)):
        print(files[l]+" : ACC "+str(accuracy_score(testlbl, classifiers[l])))
        print(files[l]+" : PRE "+str(precision_score(testlbl, classifiers[l], average="macro")))
        print(files[l]+" : REC "+str(recall_score(testlbl, classifiers[l], average="macro")))
        print(files[l]+" : F1 "+str(f1_score(testlbl, classifiers[l], average="macro")))


if __name__ == "__main__":
    main()