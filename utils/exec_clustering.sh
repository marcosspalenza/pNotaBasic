#!/bin/bash
_base="/home/spalenza/pNotaDEV/data/Beetle/"
_path=`pwd`

rm run.log

echo "Listando as atividades da base $_base"
ls $_base > atividades.txt
while read _atividade
do
        echo $_base$_atividade
        python3 pNota_main_clustering.py -i enabled -c disabled -l english -f answers.csv -s single_file "$_base$_atividade/" >> run.log
        python3 generate_trainset.py "$_base$_atividade/" >> run.log
        python3 pNota_main_classification.py -g discrete -l english -f answers.csv -s single_file "$_base$_atividade/" >> run.log
        python3 avalia_classificadores.py "$_base$_atividade/" >> run.log

done < atividades.txt