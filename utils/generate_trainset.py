import sys
import pandas as pd
from argparse import ArgumentParser # arguments manager

def main():
    parser = ArgumentParser(usage="%(prog)s [args] <datasetdir>",  description="Evaluate the pNota classifiers.")
    parser.add_argument("dataset", help="Dataset folder.")

    args = parser.parse_args()

    print("[Process] Generate Train Set")
    print(args.dataset)

    docs = []
    train = []
    grades = []

    with open(args.dataset+"logs_pNota/train", "r") as rdb:
        train = [int(tr) for tr in rdb.read().split() if tr != ""]

    if "Beetle" in args.dataset:
        beetledb = pd.read_csv(args.dataset+"answers.csv", sep=";")
        docs = beetledb["Id"].values
        grades = beetledb["Nota"].values

    elif "SciEntsBank" in args.dataset:
        sciebdb = pd.read_csv(args.dataset+"answers.csv", sep=";")
        docs = sciebdb["Id"].values
        grades = sciebdb["Nota"].values

    elif "Butcher_Jordan2010" in args.dataset:
        jordandb = pd.read_csv(args.dataset+"answers.csv", sep=";")
        docs = jordandb["Id"].values
        grades = jordandb["Nota"].values

    elif "Mohler_Mihalcea2011" in args.dataset:
        with open(args.dataset+"ave") as rdb: # me other avg
            grades = [str(g) for g in rdb.read().split("\n") if g != ""]
            docs = [str(gix) for gix, _  in enumerate(grades)]

    elif "ASAP-SAS" in args.dataset:
        dbname = args.dataset.split("/")[-2]
        asap = pd.read_csv(args.dataset+dbname+".csv", sep=";")
        docs = asap["Id"].values
        grades = asap["Nota"].values

    elif "Findes" in args.dataset:
        dbname = args.dataset.split("/")[-2]
        jordandb = pd.read_csv(args.dataset+"notas.csv", sep=";")
        docs = jordandb["Id"].values
        grades = jordandb["Nota"].values

    elif "Powergrading" in args.dataset:
        beetledb = pd.read_csv(args.dataset+"answers.csv", sep=";")
        docs = beetledb["Id"].values
        grades0 = beetledb["Nota"].values
        grades1 = beetledb["Nota2"].values
        grades2 = beetledb["Nota3"].values
        grades = [1 if sum([int(grades0[gix]), int(grades1[gix]), int(grades2[gix])]) > 1 else 0
         for gix, _ in enumerate(grades0)]

    elif "VestUFES" in args.dataset:
        dbname = args.dataset.split("/")[-3]
        path_ = "/".join(args.dataset.split("/")[:-2])
        vestdb = pd.read_csv(path_+"/Final.csv", sep=";", header=None)
        docs = vestdb[0].values
        grades = vestdb[1].values

    elif "PTASAG" in args.dataset:
        dbname = args.dataset.split("/")[-2]
        asap = pd.read_csv(args.dataset+dbname+".csv", sep=";")
        docs = asap["Id"].values
        grades = asap["Nota"].values

    else:
        print(args.dataset)
        print("Undefined dataset!")
        sys.exit()

    print(train)
    with open(args.dataset+"labels", "w") as wtr:
        wtr.write("labels\n"+"\n".join([str(g) for g in grades]))

    with open(args.dataset+"notastreino.csv", "w") as wtr:
        for dix in range(len(docs)):
            d_ = docs[dix]
            g_ = grades[dix]
            if dix in train:
                wtr.write(";".join([str(d_), str(g_)])+"\n")
            else:
                wtr.write(";".join([str(d_), "-1.0"])+"\n")

if __name__ == "__main__":
    main()
