#! /bin/bash
# basic requirements
apt-get install python3 python3-dev gcc-4.9

pip3 install numpy scipy pandas html2text unidecode matplotlib

pip3 install docker lime pybind11

pip3 install sklearn spacy nltk scikit-optimize

pip3 install git+https://github.com/IAZero/wisardpkg.git@develop

python3 -m nltk.downloader stopwords
python3 -m nltk.downloader punkt

python3 -m spacy download pt_core_news_sm
python3 -m spacy download en_core_web_sm
python3 -m spacy download es_core_news_sm
python3 -m spacy download xx_ent_wiki_sm
