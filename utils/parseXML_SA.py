import os
import pandas as pd
import xml.etree.ElementTree as ElemTree

path = ["/home/spalenza/pNotaDEV/source/naacl2012training/beetle/BeetleCore/",
        "/home/spalenza/pNotaDEV/source/naacl2012training/sciEntsBank/SciEntsBankCore/"]

# "/home/spalenza/pNotaDEV/data/naacl2012training/beetle/BeetleExtra/"
# "/home/spalenza/pNotaDEV/data/naacl2012training/sciEntsBank/SciEntsBankExtra/"
for p in path:
    for xmlfile in os.listdir(p):
        if os.path.isdir(p+"data/") == False:
            os.mkdir(p+"data/")

        if os.path.isfile(p+xmlfile) == False:
            continue

        tree = ElemTree.parse(p+xmlfile)
        root = tree.getroot()
        # print(root.tag)
        # print(root.attrib)
        idname = root.attrib["id"]
        questionText = ""
        referenceAnswers = ""
        for child in root:
            # print(child.tag, child.attrib)
            if child.tag == "questionText":
                questionText = child.text

            if child.tag == "referenceAnswers":
                referenceAnswers = [ref.text if "text" not in ref.attrib else ref.attrib["text"] for ref in child]

            if child.tag == "studentAnswers":
                students = [] 
                responses = []
                grade = []
                for ref in child:
                    students.append(ref.attrib["id"])
                    #if "answerText" in ref.attrib:
                    #    responses.append(ref.attrib["answerText"])
                    #else:
                    responses.append(ref.text)
                    grade.append(ref.attrib["accuracy"])

        print(idname)
        if os.path.isdir(p+"data/"+idname+"/") == False:
            os.mkdir(p+"data/"+idname+"/")

        # print(questionText)
        with open(p+"data/"+idname+"/question.txt", "w") as wtr:
            wtr.write(questionText)

        # print(referenceAnswers)
        with open(p+"data/"+idname+"/reference.txt", "w") as wtr:
            wtr.write("\n".join(referenceAnswers))

        df = pd.DataFrame(list(zip(responses, grade)), index=students, columns=["Resposta", "Nota"])
        df.index.names = ["Id"]
        df.to_csv(p+"data/"+idname+"/answers.csv", sep=";")