import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict

DATABASE_DIR = sys.argv[1]

LST_METRICS = ["ACC", "PRE", "REC", "F1(m)", "F1(w)", "Kappa(ln)", "Kappa(qu)"]

ORDER_BY = 0

for metric_id, metric_name in enumerate(LST_METRICS):

    fig, ax = plt.subplots()

    evaluation = []
    activities = [dir_ for dir_ in sorted(os.listdir(DATABASE_DIR)) if "atividade" in dir_ and os.path.isdir(DATABASE_DIR+"/"+dir_)]
    print(activities)
    for dir_ in activities:
        performance = []
        base_classifier = []
        for method in ["SVM", "GBC", "RDF", "DTR", "KNN", "WSD"]:
            with open("{}/{}/logs_pNota/report/{}-performance.txt".format(DATABASE_DIR, dir_, method)) as rdb:
                metrics = rdb.read().split("\n")[1:3]
                base_classifier.append(float(metrics[1].split("\t")[ORDER_BY]))
                performance.append(float(metrics[1].split("\t")[metric_id]))
        best_clf = np.argmax(base_classifier)
        evaluation.append(np.round(100*performance[best_clf], 4))
        
    bars = ax.bar(activities, evaluation, alpha=0.7, width=0.4)

    for ix, _ in enumerate(bars):
        if evaluation[ix] < 30:
            bars[ix].set_color('r')
        elif evaluation[ix] < 70:
            bars[ix].set_color('y')
        else:
            bars[ix].set_color('g')

    ax.plot([30 for _ in evaluation], color="r",  linestyle='--', linewidth=2.0, label="Insuficiente")

    ax.plot([50 for _ in evaluation], color="y", linestyle='--', linewidth=2.0, label="Adequado")

    ax.plot([70 for _ in evaluation], color="g", linestyle='--', linewidth=2.0, label="Avançado")

    ax.set_ylabel("Desempenho")
    ax.set_title(metric_name)
    ax.set_xticklabels(activities, rotation=90)
    ax.set_ylim((0, 100))

    ax.legend(loc='lower right')

    fig.tight_layout()

    plt.savefig("plot-"+metric_name)
